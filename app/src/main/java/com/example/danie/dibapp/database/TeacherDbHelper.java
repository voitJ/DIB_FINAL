package com.example.danie.dibapp.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Modella il database per il salvataggio delle informazioni sui docenti dell'universita'.
 */
public class TeacherDbHelper extends SQLiteOpenHelper {

    public final static String ID_U ="_id";
    public final static String INDIRIZZO_U = "indirizzo";
    public final static String STANZA_U = "sttanza";
    public final static String PIANO_U = "piano";
    public final static String DIPARTIMENTO_U = "dipartimento";

    public final static String ID_D = "_id";
    public final static String NOME_D = "nome";
    public final static String COGNOME_D = "cognome";
    public final static String TELEFONO_D = "telefono";
    public final static String RUOLO_D = "ruolo";
    public final static String EMAIL_D = "mail";
    public final static String UFFICIO_D = "codUff";
    public final static String FAVORITO_D = "favorito";
    public final static String PHOTO_D = "linkPhoto";

    public final static String ID_C = "_id";
    public final static String NOME_C = "nome";
    public final static String CFU_C = "cfu";
    public final static String AA_C = "aa";
    public final static String DOCENTE_C = "codDoc";
    public final static String TUTOR_C = "codTut";

    @Override
    public SQLiteDatabase getWritableDatabase() {
        return super.getWritableDatabase();
    }

    public final static String TABLE_DOCENTE = "docente";
    public final static String[] COLUMNS = { ID_D, NOME_D,COGNOME_D,TELEFONO_D, RUOLO_D, EMAIL_D,
            UFFICIO_D, FAVORITO_D, PHOTO_D};

    final private static String CREATE_CMD =

            "CREATE TABLE docente(" +
                    "_id INTEGER PRIMARY KEY, " +
                    "nome TEXT, " +
                    "cognome TEXT, " +
                    "telefono INTEGER, " +
                    "ruolo TEXT," +
                    "mail TEXT, " +
                    "codUff TEXT, " +
                    "favorito INTEGER, "+
                    "linkPhoto TEXT"   +
                    ")";  //TODO: aggiungere ufficio


    final private static String NAME = "db2";
    final private static Integer VERSION = 2;

    public TeacherDbHelper(Context context) {
        super(context, NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_CMD);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}