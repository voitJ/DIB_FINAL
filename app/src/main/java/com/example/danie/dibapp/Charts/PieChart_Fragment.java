package com.example.danie.dibapp.Charts;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.danie.dibapp.Libretto.Exam;
import com.example.danie.dibapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.listener.PieChartOnValueSelectListener;
import lecho.lib.hellocharts.model.PieChartData;
import lecho.lib.hellocharts.model.SliceValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.Chart;
import lecho.lib.hellocharts.view.PieChartView;


/**
 * A fragment containing a pie chart.
 */
public class PieChart_Fragment extends Fragment {


    private PieChartView chart;
    private PieChartData data;

    private boolean hasLabels = true;
    private boolean hasLabelsOutside = false;
    private boolean hasCenterCircle = true;
    private boolean hasCenterText1 = false;
    private boolean hasCenterText2 = false;
    private boolean isExploded = false;
    private boolean hasLabelForSelected = false;
    private int numberOf18;
    private int numberOf19;
    private int numberOf20;
    private int numberOf21;
    private int numberOf22;
    private int numberOf23;
    private int numberOf24;
    private int numberOf25;
    private int numberOf26;
    private int numberOf27;
    private int numberOf28;
    private int numberOf29;
    private int numberOf30;
    private int numberOf30L;


    final int vote18 = Color.parseColor("#fff7eb");
    final int vote19 = Color.parseColor("#ffefd8");
    final int vote20 = Color.parseColor("#ffe7c4");
    final int vote21 = Color.parseColor("#ffdfb1");
    final int vote22 = Color.parseColor("#ffd79d");
    final int vote23 = Color.parseColor("#ffcf89");
    final int vote24 = Color.parseColor("#ffc876");
    final int vote25 = Color.parseColor("#ffc062");
    final int vote26 = Color.parseColor("#ffb84e");
    final int vote27 = Color.parseColor("#ffb03b");
    final int vote28 = Color.parseColor("#ffa827");
    final int vote29 = Color.parseColor("#ffa014");
    final int vote30 = Color.parseColor("#ff9800");
    final int vote30L = Color.parseColor("#ff9800");
    private String userId;
    static FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mDatabaseReference;

    public PieChart_Fragment() {
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();


    }


    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_pie_chart, container, false);
        chart = (PieChartView) rootView.findViewById(R.id.chart);
        chart.setOnValueTouchListener(new ValueTouchListener());


        mDatabaseReference = database.getReference();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users").child(userId).child("exams");
        mDatabaseReference.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot examSnapshot : dataSnapshot.getChildren()) {

                            Exam exam = examSnapshot.getValue(Exam.class);

                            int current = exam.getGrade();

                            switch (current) {

                                case 18:
                                    numberOf18++;
                                    break;
                                case 19:
                                    numberOf19++;
                                    break;
                                case 20:
                                    numberOf20++;
                                    break;
                                case 21:
                                    numberOf21++;
                                    break;
                                case 22:
                                    numberOf22++;
                                    break;
                                case 23:
                                    numberOf23++;
                                    break;
                                case 24:
                                    numberOf24++;
                                    break;
                                case 25:
                                    numberOf25++;
                                    break;
                                case 26:
                                    numberOf26++;
                                    break;
                                case 27:
                                    numberOf27++;
                                    break;
                                case 28:
                                    numberOf28++;
                                    break;
                                case 29:
                                    numberOf29++;
                                    break;
                                case 30:
                                    numberOf30++;
                                    break;
                                case 31:
                                    numberOf30L++;
                                    break;

                            }


                        }

                        generateData();


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }


                });


        return rootView;
    }


    private void reset() {
        chart.setCircleFillRatio(1.0f);
        hasLabels = false;
        hasLabelsOutside = false;
        hasCenterCircle = false;
        hasCenterText1 = false;
        hasCenterText2 = false;
        isExploded = false;
        hasLabelForSelected = false;
    }

    private void generateData() {


        int[] votes_array = new int[]{numberOf18, numberOf19, numberOf20, numberOf21, numberOf22,
                numberOf23, numberOf24, numberOf25, numberOf26, numberOf27, numberOf28, numberOf29, numberOf30, numberOf30L};

        int[] colors = {vote18, vote19, vote20, vote21, vote22, vote23, vote24, vote25, vote26, vote27, vote28,
                vote29, vote30, vote30L};

        String[] numbers = {"18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28",
                "29", "30", "30L"};


        List<SliceValue> values = new ArrayList<SliceValue>();
        for (int i = 0; i < votes_array.length; i++) {
            SliceValue sliceValue = new SliceValue(votes_array[i], colors[i]);
            sliceValue.setLabel(numbers[i]);
            if (votes_array[i] == 0) sliceValue.setLabel("");
            values.add(sliceValue);

        }

        data = new PieChartData(values);
        data.setHasLabels(hasLabels);
        data.setHasLabelsOnlyForSelected(hasLabelForSelected);
        data.setHasLabelsOutside(hasLabelsOutside);
        data.setHasCenterCircle(hasCenterCircle);

        if (isExploded) {
            data.setSlicesSpacing(24);
        }

        if (hasCenterText1) {
            data.setCenterText1("Hello!");

            // Get roboto-italic font.
            Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Italic.ttf");
            data.setCenterText1Typeface(tf);

            // Get font size from dimens.xml and convert it to sp(library uses sp values).
            data.setCenterText1FontSize(ChartUtils.px2sp(getResources().getDisplayMetrics().scaledDensity,
                    (int) getResources().getDimension(R.dimen.pie_chart_text1_size)));
        }

        if (hasCenterText2) { //commit
            data.setCenterText2("Charts (Roboto Italic)");

            Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "Roboto-Italic.ttf");

            data.setCenterText2Typeface(tf);
            data.setCenterText2FontSize(ChartUtils.px2sp(getResources().getDisplayMetrics().scaledDensity,
                    (int) getResources().getDimension(R.dimen.pie_chart_text2_size)));
        }

        chart.setPieChartData(data);
    }


    private void explodeChart() {
        isExploded = !isExploded;
        generateData();

    }

    private void toggleLabelsOutside() {
        // has labels have to be true:P
        hasLabelsOutside = !hasLabelsOutside;
        if (hasLabelsOutside) {
            hasLabels = true;
            hasLabelForSelected = false;
            chart.setValueSelectionEnabled(hasLabelForSelected);
        }

        if (hasLabelsOutside) {
            chart.setCircleFillRatio(0.7f);
        } else {
            chart.setCircleFillRatio(1.0f);
        }

        generateData();

    }

    private void toggleLabels() {
        hasLabels = !hasLabels;

        if (hasLabels) {
            hasLabelForSelected = false;
            chart.setValueSelectionEnabled(hasLabelForSelected);

            if (hasLabelsOutside) {
                chart.setCircleFillRatio(0.7f);
            } else {
                chart.setCircleFillRatio(1.0f);
            }
        }

        generateData();
    }

    private void toggleLabelForSelected() {
        hasLabelForSelected = !hasLabelForSelected;

        chart.setValueSelectionEnabled(hasLabelForSelected);

        if (hasLabelForSelected) {
            hasLabels = false;
            hasLabelsOutside = false;

            if (hasLabelsOutside) {
                chart.setCircleFillRatio(0.7f);
            } else {
                chart.setCircleFillRatio(1.0f);
            }
        }

        generateData();
    }

    /**
     * To animate values you have to change targets values and then call {@link Chart#startDataAnimation()}
     * method(don't confuse with View.animate()).
     */
    private void prepareDataAnimation() {
        for (SliceValue value : data.getValues()) {
            value.setTarget((float) Math.random() * 30 + 15);

        }
    }

    private class ValueTouchListener implements PieChartOnValueSelectListener {

        @Override
        public void onValueSelected(int arcIndex, SliceValue value) {


        }

        @Override
        public void onValueDeselected() {
            // TODO Auto-generated method stub

        }

    }


}



