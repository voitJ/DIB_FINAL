package com.example.danie.dibapp.Libretto;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.danie.dibapp.R;

import java.util.List;

/**
 * Created by Alessandro on 16/01/17.
 */

public class ExamAdapter extends RecyclerView.Adapter<ExamAdapter.ViewHolder> {
    private List<Exam> examList;
    private static Context context;

    public ExamAdapter(Context context, List<Exam> examList) {
        this.context = context;
        this.examList = examList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.exam_item, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Exam exam = examList.get(position);
        holder.name.setText(String.valueOf(exam.getName()));
        holder.grade.setText(String.valueOf(exam.getGrade()));
        holder.credits.setText(String.valueOf(exam.getCredits()));
        holder.date.setText(String.valueOf(exam.getDate()));
    }

    @Override
    public int getItemCount() {
        return examList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        TextView name;
        TextView grade;
        TextView credits;
        TextView date;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.examName);
            grade = (TextView) v.findViewById(R.id.examGrade);
            credits = (TextView) v.findViewById(R.id.examCredits);
            date = (TextView) v.findViewById(R.id.examDate);

        }

        @Override
        public void onClick(View view) {
            // Context context = view.getContext();
            // mArticle.getName()
        }

        @Override
        public boolean onLongClick(View view) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View v = inflater.inflate(R.layout.bottom_sheet, null);
            TextView txtModifica = (TextView) v.findViewById(R.id.txt_modifica);
            TextView txtElimina = (TextView) v.findViewById(R.id.txt_bottom_vote);


            final Dialog mBottomSheetDialog = new Dialog(context,
                    R.style.MaterialDialogSheet);
            mBottomSheetDialog.setContentView(view);
            mBottomSheetDialog.setCancelable(true);
            mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
            mBottomSheetDialog.show();


            txtModifica.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(v.getContext(), EditExamActivity.class);
                    intent.putExtra("name", name.getText());
                    intent.putExtra("grade", grade.getText());
                    intent.putExtra("credits", credits.getText());
                    intent.putExtra("date", date.getText());
                    context.startActivity(intent);
                    mBottomSheetDialog.dismiss();
                }
            });

            txtElimina.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {


                }
            });
            return true;
        }
    }
}

