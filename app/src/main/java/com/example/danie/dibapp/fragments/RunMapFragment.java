package com.example.danie.dibapp.fragments;

import android.Manifest;
import android.support.v4.app.Fragment;
import android.app.SearchManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.example.danie.dibapp.googlePlaces.PlacesReaded;
import com.example.danie.dibapp.MainActivity;
import com.example.danie.dibapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class RunMapFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = RunMapFragment.class.getName();
    private static final String API_KEY = "AIzaSyCPiBo8Wr0O7DjiBtCAEfR4nOZVmOLRfdY";

    String[] spinnerArray;

    public static GoogleMap mMap;
    private SupportMapFragment sMapFragment;
    private static final float cameraZoom = 15;
    private static final LatLng dib = new LatLng(41.109732, 16.881521);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG, "entrato nel metodo onCreate");

        //MenuItem searchItem = MainActivity.toolbar.getMenu().findItem(R.id.menu_search);
        //SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        //if(searchView != null){
        //    setSearchView(searchView);
        //}

        spinnerArray = getResources().getStringArray(R.array.map_options);
        Spinner spinner = (Spinner) getActivity().findViewById(R.id.menu_spinner);
        setSpinnerListener(spinner);

        sMapFragment = SupportMapFragment.newInstance();
        sMapFragment.getMapAsync(this);
        MainActivity.sFm.beginTransaction().add(R.id.map_frame, sMapFragment).commit();
        MainActivity.sFm.beginTransaction().show(sMapFragment).commit();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "entrato nel metodo onCreateView");
        ((MainActivity) getActivity())
                .setActionBarTitle("Mappa");
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    private void getNearbyPlaces(String type) {
        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        sb.append("location=" + 41.109732 + "," + 16.881521);
        sb.append("&radius=500");
        sb.append("&types=" + type);
        sb.append("&sensor=true");
        sb.append("&key=" + API_KEY);

        PlacesReaded placesReaded = new PlacesReaded();
        placesReaded.execute(sb.toString());
    }

    private void setSpinnerListener(Spinner spinner){
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (spinnerArray[position]) {
                    case "DIB":
                        mMap.clear();
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dib, cameraZoom));
                        mMap.addMarker(new MarkerOptions().position(dib).title("Dipartimento di Informatica"));
                        break;
                    case "Banca":
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dib, cameraZoom));
                        getNearbyPlaces("bank");
                        break;
                    case "Bar":
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dib, cameraZoom));
                        getNearbyPlaces("bar");
                        break;
                    case "Cartolerie":
                        mMap.clear();
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dib, cameraZoom));
                        mMap.addMarker(new MarkerOptions().position(new LatLng(41.107588, 16.877398)).title("Cartoleria Masciopinto Antonio"));
                        mMap.addMarker(new MarkerOptions().position(new LatLng(41.110755, 16.876584)).title("Arte & Disegno Fortunato S.R.L."));
                        mMap.addMarker(new MarkerOptions().position(new LatLng(41.103439, 16.879259)).title("Giuseppe Tanzi & figli sas"));
                        mMap.addMarker(new MarkerOptions().position(new LatLng(41.113580, 16.875975)).title("Cartoleria Arena Di Raspatelli Giacomo"));
                        break;
                    case "Librerie":
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dib, cameraZoom));
                        getNearbyPlaces("library");
                        break;
                    case "Mangiare":
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dib, cameraZoom));
                        getNearbyPlaces("food");
                        break;
                    case "Palestra":
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dib, cameraZoom));
                        getNearbyPlaces("gym");
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.i(TAG, "Non è stato selezionato nulla nello Spinner");
            }
        });
    }

    private void setSearchView(SearchView searchView){
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));

        searchView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Search clicked!");
            }
        });
        searchView.setOnCloseListener(new SearchView.OnCloseListener(){
            @Override
            public boolean onClose() {
                Log.d(TAG, "Search closed!");
                return false;
            }
        });
        searchView.setOnSearchClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Search Click!");
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(TAG, "Query Text Submit!" + query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d(TAG, "Query Text Changed!" + newText);
                return false;
            }
        });
        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener(){
            @Override
            public boolean onSuggestionSelect(int position) {
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                return false;
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(dib, cameraZoom));
        mMap.addMarker(new MarkerOptions().position(dib).title("Dipartimento di Informatica"));
        mMap.getUiSettings().setZoomGesturesEnabled(false);
    }
}
