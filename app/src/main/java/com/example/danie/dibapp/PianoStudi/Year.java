package com.example.danie.dibapp.PianoStudi;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by Alessandro on 22/01/17.
 */

public class Year extends ExpandableGroup<Course> {

    public Year(String title, List<Course> items) {
        super(title, items);
    }
}
