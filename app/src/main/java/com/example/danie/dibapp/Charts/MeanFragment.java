package com.example.danie.dibapp.Charts;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import com.example.danie.dibapp.R;
import com.example.danie.dibapp.Libretto.Exam;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lecho.lib.hellocharts.formatter.SimpleLineChartValueFormatter;
import lecho.lib.hellocharts.listener.LineChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.model.ValueShape;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;

public class MeanFragment extends Fragment {
    private LineChartView chart;
    private LineChartData data;
    private int numberOfPoints;
    private boolean hasAxes = true;
    private boolean hasAxesNames = false;
    private boolean hasPoints = true;
    private ValueShape shape = ValueShape.CIRCLE;
    private boolean hasLines = true;
    private boolean isFilled = false;
    private boolean hasLabels = true;
    private boolean isCubic = false;
    private boolean hasLabelForSelected = false;
    final int weighted_average_Color = Color.parseColor("#ffa827");
    final int average_Color = Color.parseColor("#00796B");
    private CheckBox ch1, ch2;
    private float[] weighted_average;
    private float[] average;
    private ArrayList<Integer> grades = new ArrayList<Integer>();
    private ArrayList<Integer> cfu = new ArrayList<Integer>();
    private ArrayList<String> dates = new ArrayList<String>();
    private DecimalFormat df = new DecimalFormat("#.##");
    private SimpleDateFormat dbFormat = new SimpleDateFormat("yyyy/MM/dd");
    SimpleDateFormat listFormat = new SimpleDateFormat("dd/MM/yy");
    private String userId;
    static FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mDatabaseReference;

    public MeanFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_weighted_mean, container, false);
        df.setMinimumFractionDigits(2);
        chart = (LineChartView) rootView.findViewById(R.id.chart2);
        chart.setOnValueTouchListener(new ValueTouchListener());
        ch1 = (CheckBox) rootView.findViewById(R.id.ch1);
        ch2 = (CheckBox) rootView.findViewById(R.id.ch2);


        mDatabaseReference = database.getReference();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users").child(userId).child("exams");
        mDatabaseReference.orderByChild("date").addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot examSnapshot : dataSnapshot.getChildren()) {

                            Exam exam = examSnapshot.getValue(Exam.class);
                            if (exam.getGrade() != 17) {
                                int grade = exam.getGrade();
                                if (grade == 31) grade = 30;
                                grades.add(grade);
                                cfu.add(exam.getCredits());
                                String date = exam.getDate();
                                Date data = null;
                                try {
                                    data = dbFormat.parse(date);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                date = listFormat.format(data);
                                dates.add(date);

                            }


                        }

                        numberOfPoints = grades.size();
                        weighted_average = new float[numberOfPoints];
                        average = new float[numberOfPoints];


                        for (int i = 0; i < numberOfPoints; i++) {
                            weighted_average[i] = weighted_mean(grades, cfu, i);
                            average[i] = average(grades, i);
                        }

                        generateData();
                        chart.setViewportCalculationEnabled(false);
                        resetViewport();
                        ch1.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                generateData();
                            }
                        });
                        ch2.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                generateData();
                            }
                        });

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }


                });


        return rootView;
    }


    private float average(ArrayList<Integer> grades, int actual_dim) {

        int temp = actual_dim + 1;
        int current_val = 0;
        for (int i = 0; i <= temp - 1; i++) {
            current_val = current_val + (grades.get(i));

        }
        df.format(current_val / temp);
        return (float) current_val / temp;
    }

    private float weighted_mean(ArrayList<Integer> grades, ArrayList<Integer> cfu, int actual_dim) {


        int current_val = 0;
        int tot_cfu = 0;
        for (int i = 0; i <= actual_dim; i++) {
            current_val = current_val + (grades.get(i) * cfu.get(i));
            tot_cfu = tot_cfu + cfu.get(i);
        }
        df.format(current_val / tot_cfu);
        return (float) current_val / tot_cfu;
    }



    private void generateData() {


        List<Line> lines = new ArrayList<Line>();


        List<PointValue> average_values = new ArrayList<PointValue>();
        for (int j = 0; j < numberOfPoints; ++j) {
            average_values.add(new PointValue(j, average[j]));
        }
        Line average_line = new Line(average_values);
        average_line.setColor(average_Color);
        average_line.setShape(shape);
        average_line.setCubic(isCubic);
        average_line.setFilled(isFilled);
        average_line.setHasLabels(hasLabels);
        average_line.setFormatter(new SimpleLineChartValueFormatter(2));
        average_line.setHasLabelsOnlyForSelected(hasLabelForSelected);
        average_line.setHasLines(hasLines);
        average_line.setHasPoints(hasPoints);


        List<PointValue> weighted_average_values = new ArrayList<PointValue>();
        for (int j = 0; j < numberOfPoints; ++j) {
            weighted_average_values.add(new PointValue(j, weighted_average[j]));
        }
        Line weighted_average_line = new Line(weighted_average_values);
        weighted_average_line.setColor(weighted_average_Color);
        weighted_average_line.setShape(shape);
        weighted_average_line.setCubic(isCubic);
        weighted_average_line.setFilled(isFilled);
        weighted_average_line.setHasLabels(hasLabels);
        weighted_average_line.setFormatter(new SimpleLineChartValueFormatter(2));
        weighted_average_line.setHasLabelsOnlyForSelected(hasLabelForSelected);
        weighted_average_line.setHasLines(hasLines);
        weighted_average_line.setHasPoints(hasPoints);
        lines.add(weighted_average_line);
        lines.add(average_line);

        data = new LineChartData(lines);
        if (hasAxes) {
            List<AxisValue> axisXValuesWeight = new ArrayList<>();
            for (int i = 0; i < numberOfPoints; i++)
                axisXValuesWeight.add(new AxisValue(i).setLabel(dates.get(i)));
            Axis axisX = new Axis(axisXValuesWeight);
            axisX.setHasLines(true);
            axisX.setMaxLabelChars(8);
            axisX.setHasTiltedLabels(true);
            List<AxisValue> axisValuesWeight = new ArrayList<>();
            for (int i = 18; i < 31; i++)
                axisValuesWeight.add(new AxisValue(i));
            Axis axisY = new Axis(axisValuesWeight);
            axisY.setHasLines(true);
            if (hasAxesNames) {
                axisX.setName("Axis X");
                axisY.setName("Axis Y");
            }
            data.setAxisXBottom(axisX);
            data.setAxisYLeft(axisY);
        } else {
            data.setAxisXBottom(null);
            data.setAxisYLeft(null);
        }
        chart.setLineChartData(data);


        if (ch1.isChecked()) {
            weighted_average_line.setHasLines(true);
            weighted_average_line.setHasPoints(true);
        } else {

            weighted_average_line.setHasLines(false);
            weighted_average_line.setHasPoints(false);

        }


        if (ch2.isChecked()) {
            average_line.setHasLines(true);
            average_line.setHasPoints(true);
        } else {

            average_line.setHasLines(false);
            average_line.setHasPoints(false);

        }


    }

    private class ValueTouchListener implements LineChartOnValueSelectListener {
        @Override
        public void onValueSelected(int lineIndex, int pointIndex, PointValue value) {
            openBottomSheet(value.getX(), value.getY());
        }

        public void openBottomSheet(float x, float y) {

        }

        @Override
        public void onValueDeselected() {
            // TODO Auto-generated method stub
        }
    }

    private void resetViewport() {
        // Reset viewport height range to (0,100)
        final Viewport v = new Viewport(chart.getMaximumViewport());
        v.bottom = 18;
        v.top = 30;
        v.left = 0;
        if (numberOfPoints == 0) v.right = numberOfPoints + 1;
        else if (numberOfPoints == 1) v.right = numberOfPoints;
        else v.right = numberOfPoints - 1;
        chart.setMaximumViewport(v);
        chart.setCurrentViewport(v);

    }
}