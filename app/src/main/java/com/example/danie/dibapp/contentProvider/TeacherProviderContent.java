package com.example.danie.dibapp.contentProvider;


import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;
import com.example.danie.dibapp.SearchActivity;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import com.example.danie.dibapp.Data.TeacherData;

public class TeacherProviderContent extends ContentProvider {


    private static boolean bn = false; //controlla se ci sono elementi nella lista cercata

    public static boolean isValidSearch() {
        return bn;
    }

    @Override
    public boolean onCreate() {
        return false;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {

        //ripristino il flag
        bn = false;

        MatrixCursor cursor = new MatrixCursor(
                new String[] {
                        BaseColumns._ID,
                        SearchManager.SUGGEST_COLUMN_TEXT_1,
                        SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID
                });

        //array contenente i docenti
        JsonArray jsArray = SearchActivity.arrayJson;

        //testo digitato dall'utente
        String query = uri.getLastPathSegment().toLowerCase();

        //limite teorico della lista da visulizzare
        int limit = Integer.parseInt(uri.getQueryParameter(SearchManager.SUGGEST_PARAMETER_LIMIT));

        for (JsonElement jsonElem:jsArray) {

            String id = jsonElem.getAsJsonObject().get(TeacherData.TAG_ID).getAsString();
            String nome = jsonElem.getAsJsonObject().get(TeacherData.TAG_NOME).getAsString();
            String cognome = jsonElem.getAsJsonObject().get(TeacherData.TAG_COGNOME).getAsString();
            StringBuilder str = new StringBuilder(50);
            StringBuilder str1 = new StringBuilder(50);
            str.append(cognome);
            str1.append(nome);
            str.append(" ");
            str1.append(" ");
            str.append(nome);
            str1.append(cognome);

            if (str.toString().toLowerCase().contains(query) ||
                    str1.toString().toLowerCase().contains(query)) {
                cursor.addRow(new Object[] {id, new String(str1.toString()),id});
                    bn =true;

            //se il cursore ha raggiunto il limite di righe della ListView, esci dal ciclo.
            }else if (cursor.getCount() >= limit) {
                break;
            }
        }
        Log.i("Tag",Boolean.toString(bn));
        return cursor;
    }
}
