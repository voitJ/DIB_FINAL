package com.example.danie.dibapp.utility;

import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by Alessandro on 11/02/17.
 */

public class MyDatabaseUtil {

    private static FirebaseDatabase mDatabase;

    public static FirebaseDatabase getDatabase() {
        if (mDatabase == null) {
            mDatabase = FirebaseDatabase.getInstance();
            mDatabase.setPersistenceEnabled(true);

        }

        return mDatabase;

    }

}