package com.example.danie.dibapp.utility;

import com.example.danie.dibapp.Data.OnselectAction;

import java.io.Serializable;

/**
 * Claase wrap che permette di serializzare un'interfaccia.
 */
public class wrapInterface implements Serializable {
    OnselectAction interfaceCallBack;

    public wrapInterface(OnselectAction intfce) {
        this.interfaceCallBack = intfce;
    }

    public OnselectAction getInterfaceCallBack() {
        return interfaceCallBack;
    }
}