package com.example.danie.dibapp.Libretto;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.danie.dibapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class ExamDetailsActivity extends AppCompatActivity {

    private ImageView average_trending;
    private ImageView weighted_average_trending;
    private String examString = "vuoto";
    private String gradeString;
    private String creditsString;
    private String dateString;
    private String professorString;
    private String notesString;
    private String userId;
    private float currAverage;
    private float currWeightedAverage;
    private int numExams;
    ArrayList<Exam> mExamArrayList;
    static FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mDatabaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent() != null) {

            Intent i = getIntent();
            examString = i.getStringExtra("name");
            dateString = i.getStringExtra("date");
            gradeString = i.getStringExtra("grade");
            creditsString = i.getStringExtra("credits");
            professorString = i.getStringExtra("professor");
            notesString = i.getStringExtra("notes");


        }

        if (creditsString.length() == 6) {
            creditsString = creditsString.substring(0, 2);
        } else {
            creditsString = String.valueOf(creditsString.charAt(0));
        }

        setContentView(R.layout.activity_dettagli_esame);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar2);
        myToolbar.setTitle(examString);
        myToolbar.setSubtitle(dateString);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);

        final TextView grade = (TextView) findViewById(R.id.vote_textview_value);
        grade.setText(gradeString);
        if (gradeString.equals(" L")) {
            grade.setText("30 e lode");
            gradeString = "30";
        } else if (gradeString.equals(" ID")) {
            grade.setText("Idoneo");
            gradeString = "17";
        }


        final TextView credits = (TextView) findViewById(R.id.cfu_textview_value);
        final TextView professor = (TextView) findViewById(R.id.professor_textview_value);
        final TextView notes = (TextView) findViewById(R.id.notes_textview_value);
        final TextView averageInfluence = (TextView) findViewById(R.id.average_influence);
        final TextView weightedAverageInfluence = (TextView) findViewById(R.id.weighted_average_influence);
        final TextView previousAverage = (TextView) findViewById(R.id.start_average);
        final TextView currentAverage = (TextView) findViewById(R.id.end_average);
        final TextView previousWeightedAverage = (TextView) findViewById(R.id.start_weighted_average);
        final TextView currentWeightedverage = (TextView) findViewById(R.id.end_weighted_average);
        average_trending = (ImageView) findViewById(R.id.average_trending);
        weighted_average_trending = (ImageView) findViewById(R.id.weighted_average_trending);


        mExamArrayList = new ArrayList<Exam>();
        mDatabaseReference = database.getReference();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users").child(userId).child("exams");
        mDatabaseReference.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot examSnapshot : dataSnapshot.getChildren()) {

                            Exam exam = examSnapshot.getValue(Exam.class);
                            mExamArrayList.add(exam);

                        }
                        int mSumGrades = 0;
                        int mSumCredits = 0;
                        int mSumGradesXCredits = 0;
                        int mAchievedCredits = 0;
                        for (Exam exam : mExamArrayList) {
                            mAchievedCredits = mAchievedCredits + exam.getCredits();
                            if (exam.getGrade() != 17) {
                                numExams++;
                                int grade = exam.getGrade();
                                if (grade == 31) grade = 30;
                                mSumGrades = mSumGrades + grade;
                                mSumCredits = mSumCredits + exam.getCredits();
                                mSumGradesXCredits = mSumGradesXCredits + grade * exam.getCredits();


                            }

                        }

                        currAverage = 1.0f * mSumGrades / numExams;
                        currWeightedAverage = 1.0f * mSumGradesXCredits / mSumCredits;
                        if (Double.isNaN(currAverage)) currAverage = 0.0f;
                        if (Double.isNaN(currWeightedAverage)) currWeightedAverage = 0.0f;
                        String formatCurrentAverage = String.format("%,.2f", currAverage);
                        String formattedCurrentWeightedAverage = String.format("%,.2f", currWeightedAverage);
                        currentAverage.setText(formatCurrentAverage);
                        currentWeightedverage.setText(formattedCurrentWeightedAverage);


                        float prevAverage = (mSumGrades - Float.parseFloat(gradeString)) / (numExams - 1);
                        if (gradeString.equals("17")) prevAverage = currAverage;
                        if (Float.isNaN(prevAverage)) prevAverage = 0.0f;
                        if (Float.isInfinite(prevAverage)) prevAverage = 100f;
                        String formatPreviousAverage = String.format("%,.2f", prevAverage);
                        previousAverage.setText(formatPreviousAverage);


                        float prevWeightedAverage = (mSumGradesXCredits - Float.parseFloat(gradeString) * Float.parseFloat(creditsString)) / (mSumCredits - Float.parseFloat(creditsString));
                        if (gradeString.equals("17")) prevWeightedAverage = currWeightedAverage;
                        if (Float.isNaN(prevWeightedAverage)) prevWeightedAverage = 0.0f;
                        if (Float.isInfinite(prevWeightedAverage)) prevWeightedAverage = 100f;
                        String formatPreviousWeightedAverage = String.format("%,.2f", prevWeightedAverage);
                        previousWeightedAverage.setText(formatPreviousWeightedAverage);


                        float averageVariance = (currAverage / prevAverage * 100f) - 100f;
                        if (Float.isNaN(averageVariance)) averageVariance = 0.0f;
                        if (Float.isInfinite(averageVariance)) averageVariance = 100f;

                        float weightedAverageVariance = (currWeightedAverage / prevWeightedAverage * 100f) - 100f;
                        if (Float.isNaN(weightedAverageVariance)) weightedAverageVariance = 0.0f;
                        if (Float.isInfinite(weightedAverageVariance))
                            weightedAverageVariance = 100f;

                        String formatAverageInfluence = String.format("%,.2f", averageVariance);
                        String formatWeightedAverageInfluence = String.format("%,.2f", weightedAverageVariance);


                        if (averageVariance > 0f) {
                            formatAverageInfluence = "+" + formatAverageInfluence;
                            average_trending.setBackgroundResource(R.drawable.ic_trending_up);
                            averageInfluence.setTextColor(getResources().getColor(R.color.trendingUp));
                        } else if (averageVariance < 0f) {
                            average_trending.setBackgroundResource(R.drawable.ic_trending_down);
                            averageInfluence.setTextColor(getResources().getColor(R.color.trendingDown));
                        }


                        if (weightedAverageVariance > 0f) {
                            formatWeightedAverageInfluence = "+" + formatWeightedAverageInfluence;
                            weighted_average_trending.setBackgroundResource(R.drawable.ic_trending_up);
                            weightedAverageInfluence.setTextColor(getResources().getColor(R.color.trendingUp));
                        } else if (weightedAverageVariance < 0f) {
                            weighted_average_trending.setBackgroundResource(R.drawable.ic_trending_down);
                            weightedAverageInfluence.setTextColor(getResources().getColor(R.color.trendingDown));
                        }


                        averageInfluence.setText(formatAverageInfluence + "%");
                        weightedAverageInfluence.setText(formatWeightedAverageInfluence + "%");
                        credits.setText(creditsString);
                        professor.setText(professorString);
                        notes.setText(notesString);


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }
                });


    }


    public boolean onOptionsItemSelected(MenuItem item) {

        this.finish();


        return true;
    }


}
