package com.example.danie.dibapp.fragments;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.danie.dibapp.Authentication.LoginActivity;
import com.example.danie.dibapp.Authentication.SignupActivity;
import com.example.danie.dibapp.Libretto.Exam;
import com.example.danie.dibapp.MainActivity;
import com.example.danie.dibapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static com.facebook.login.widget.ProfilePictureView.TAG;


public class DashboardFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    ArrayList<Exam> mExamArrayList;
    private String mParam1;
    private String mParam2;
    static FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mDatabaseReference;
    private float mAverage;
    private float mWeightedAverage;
    private int numExams;
    private static final int RESULT_LOAD_IMAGE = 2;
    private CircleImageView profile;
    private SharedPreferences pref;
    private String userId;
    //private static final String PREFS_KEY = "profilePic";
    private static final String SHARED_PREFERENCES_TYPE = "Account";
    private OnFragmentInteractionListener mListener;
    private Uri photoUrl;
    FirebaseUser user;
    DateFormat dbFormat = new SimpleDateFormat("yyyy/MM/dd");

    public DashboardFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DashboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DashboardFragment newInstance(String param1, String param2) {

        DashboardFragment fragment = new DashboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user.getUid() != null) {
            userId = user.getUid();
            pref = this.getActivity().getSharedPreferences("Profile", MODE_PRIVATE);
        }

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {



        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ((MainActivity) getActivity())
                .setActionBarTitle("Dashboard");
        setHasOptionsMenu(true);

        profile = (CircleImageView) view.findViewById(R.id.user_profile_photo);
        profile.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,
                        "Select Picture"), RESULT_LOAD_IMAGE);
            }
        });

        ImageView background = (ImageView) view.findViewById(R.id.header_cover_image);
        background.setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.DST_ATOP);
        final TextView userFullName = (TextView) view.findViewById(R.id.user_profile_name);
        final ProgressBar cfu_progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        final ProgressBar vote_projection_progressBar = (ProgressBar) view.findViewById(R.id.progressBar2);
        final TextView progressingTextView = (TextView) view.findViewById(R.id.progress_circle_text);
        final TextView achievedCFUTextView = (TextView) view.findViewById(R.id.achievedCFU);
        final TextView completedExams_text = (TextView) view.findViewById(R.id.completed_exams_value);
        final TextView average_text = (TextView) view.findViewById(R.id.average_value);
        final TextView weighted_average_text = (TextView) view.findViewById(R.id.weighted_average_value);
        final TextView projection = (TextView) view.findViewById(R.id.projection);
        final TextView vote_projection = (TextView) view.findViewById(R.id.vote_projection);
        final TextView minAverageTextView = (TextView) view.findViewById(R.id.minAverageValue);
        final TextView maxAverageTextView = (TextView) view.findViewById(R.id.maxAverageValue);
        final TextView minProjectionTextView = (TextView) view.findViewById(R.id.minProjection);
        final TextView maxProjectionTextView = (TextView) view.findViewById(R.id.maxProjection);
        final TextView lastActivity = (TextView) view.findViewById(R.id.last_activity_value);
        final TextView daysDifference = (TextView) view.findViewById(R.id.days_difference);
        final LinearLayout ll = (LinearLayout) view.findViewById(R.id.ll);

        photoUrl = Uri.parse("https://firebasestorage.googleapis.com/v0/b/dibapp-b8fda.appspot.com/o/profile_icon.png?alt=media&token=cf3742c4-acb6-4c2d-b2ae-a516569aa082");
        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null && user.getPhotoUrl() != null) {
            photoUrl = user.getPhotoUrl();
        }

        pref = getContext().getSharedPreferences(SHARED_PREFERENCES_TYPE, MODE_PRIVATE);
        String localStr = pref.getString(userId, null);
        if (localStr != null) {
            Uri localUrl = Uri.parse(localStr);
            if (localUrl != photoUrl) {
                photoUrl = localUrl;
                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                        .setPhotoUri(localUrl)
                        .build();
                user.updateProfile(profileUpdates);

            }

        }

        Glide.with(this)
                .load(photoUrl)
                //.skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(profile);


        mExamArrayList = new ArrayList<Exam>();
        mDatabaseReference = database.getReference();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users").child(userId).child("exams");
        mDatabaseReference.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot examSnapshot : dataSnapshot.getChildren()) {

                            Exam exam = examSnapshot.getValue(Exam.class);
                            mExamArrayList.add(exam);

                        }
                        completedExams_text.setText(Integer.toString(mExamArrayList.size()));
                        int mSumGrades = 0;
                        int mSumCredits = 0;
                        int mSumGradesXCredits = 0;
                        int mAchievedCredits = 0;
                        for (Exam exam : mExamArrayList) {
                            mAchievedCredits = mAchievedCredits + exam.getCredits();
                            if (exam.getGrade() != 17) {
                                numExams++;
                                int grade = exam.getGrade();
                                if (grade == 31) grade = 30;
                                mSumGrades = mSumGrades + grade;
                                mSumCredits = mSumCredits + exam.getCredits();
                                mSumGradesXCredits = mSumGradesXCredits + grade * exam.getCredits();


                            }

                        }

                        mAverage = 1.0f * mSumGrades / numExams;
                        mWeightedAverage = 1.0f * mSumGradesXCredits / mSumCredits;
                        if (Double.isNaN(mAverage)) mAverage = 0.0f;
                        if (Double.isNaN(mWeightedAverage)) mWeightedAverage = 0.0f;

                        userFullName.setText(user.getDisplayName());
                        if (user.getDisplayName() == null)
                            userFullName.setText(SignupActivity.getFullname());
                        String mFormattedAverage = String.format("%,.2f", mAverage);
                        String mFormattedWeightedAverage = String.format("%,.2f", mWeightedAverage);
                        average_text.setText(mFormattedAverage);
                        weighted_average_text.setText(mFormattedWeightedAverage);
                        float cfu_percentage = ((float) mAchievedCredits * 100f) / 180f;
                        achievedCFUTextView.setText(Integer.toString(mAchievedCredits) + "/180");
                        cfu_progressBar.setProgress(mAchievedCredits);
                        progressingTextView.setText("" + Integer.toString((int) cfu_percentage) + " %");
                        achievedCFUTextView.setText(Integer.toString(mAchievedCredits) + "/180");
                        vote_projection.setText(Long.toString(Math.round(mWeightedAverage * 11 / 3)) + "/110");
                        vote_projection_progressBar.setProgress((int) Math.round(mWeightedAverage * 11 / 3));
                        int missingCredits = 180 - mAchievedCredits;
                        String missingCreditsStr = Integer.toString(missingCredits);
                        projection.setText("Proiezioni basate sui rimanenti " + missingCreditsStr + " crediti");
                        float minAverage = 1.0f * (mSumGradesXCredits + (missingCredits * 18)) / 180;
                        String mFormattedminAverage = String.format("%,.2f", minAverage);
                        minAverageTextView.setText(mFormattedminAverage);
                        minProjectionTextView.setText(Long.toString(Math.round(minAverage * 11 / 3)) + "/110");
                        float maxAverage = 1.0f * (mSumGradesXCredits + (missingCredits * 30)) / 180;
                        String mFormattedmaxAverage = String.format("%,.2f", maxAverage);
                        maxAverageTextView.setText(mFormattedmaxAverage);
                        maxProjectionTextView.setText(Long.toString(Math.round(maxAverage * 11 / 3)) + "/110");

                        if (!mExamArrayList.isEmpty()) {
                            Date date1 = null;
                            lastActivity.setText("Superamento esame di " + mExamArrayList.get(mExamArrayList.size() - 1).getName() + " con voto " + mExamArrayList.get(mExamArrayList.size() - 1).getGrade());
                            try {
                                date1 = dbFormat.parse(mExamArrayList.get(mExamArrayList.size() - 1).getDate().toString());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            Date date = new Date();
                            long diff = date.getTime() - date1.getTime();
                            daysDifference.setText(Long.toString(TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)) + " giorni fa");


                        } else {
                            lastActivity.setText(R.string.no_activity);
                            ll.setVisibility(View.GONE);

                        }

                        DatabaseReference mNewDatabaseReference = FirebaseDatabase.getInstance().getReference();
                        mNewDatabaseReference.child("users").child(userId).child("stats").child("average").setValue(mFormattedAverage);
                        mNewDatabaseReference.child("users").child(userId).child("stats").child("weighted_average").setValue(mFormattedWeightedAverage);
                        mNewDatabaseReference.child("users").child(userId).child("stats").child("num_idoneita").setValue(Integer.toString(numExams));


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // databaseError
                    }


                });

        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.sign_out, menu);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_signout:
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;

            default:

                return super.onOptionsItemSelected(item);

        }
        return false;
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri imageUri = data.getData();
            profile.setImageURI(imageUri);
            uploadFile(imageUri);

        }

    }


    private void uploadFile(final Uri imageUri) {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();
        // StorageReference profilePicRef = storageRef.child(userId);
        //UploadTask uploadTask = profilePicRef.putFile(imageUri);

        pref = getContext().getSharedPreferences(SHARED_PREFERENCES_TYPE, MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(userId, imageUri.toString());
        editor.commit();
        Log.d(TAG, "Inserita nella shared " + imageUri.toString());


        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setPhotoUri(imageUri)
                .build();
        user.updateProfile(profileUpdates);

    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}