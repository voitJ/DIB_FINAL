package com.example.danie.dibapp.Data;

import android.graphics.Bitmap;

public interface OnselectAction {
    void FavoriteAddOrRemove(String name, String surname, String ruolo, boolean isFavorite, String id,
                             String photo, Bitmap s);
    void OnListenerItemSelection(String id);
}

