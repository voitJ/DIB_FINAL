package com.example.danie.dibapp.PianoStudi;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Alessandro on 21/01/17.
 */

public class Course implements Parcelable {

    String name;
    String professor;
    int credits;
    int year;


    public Course(String name, String professor, int credits, int year) {
        this.name = name;
        this.professor = professor;
        this.credits = credits;
        this.year = year;
    }

    public Course() {
    }

    protected Course(Parcel in) {
        name = in.readString();
        professor = in.readString();
        credits = in.readInt();
        year = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(professor);
        dest.writeInt(credits);
        dest.writeInt(year);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Course> CREATOR = new Creator<Course>() {
        @Override
        public Course createFromParcel(Parcel in) {
            return new Course(in);
        }

        @Override
        public Course[] newArray(int size) {
            return new Course[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
