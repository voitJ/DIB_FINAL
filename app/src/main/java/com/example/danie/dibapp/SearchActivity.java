package com.example.danie.dibapp;


import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.danie.dibapp.Data.OnselectAction;
import com.example.danie.dibapp.Data.TeacherData;
import com.example.danie.dibapp.clientConnectionUtility.ServerConnection;
import com.example.danie.dibapp.contentProvider.TeacherProviderContent;
import com.example.danie.dibapp.database.TeacherDbHelper;
import com.example.danie.dibapp.fragments.FavoriteFragment;
import com.example.danie.dibapp.fragments.ProfFragment;
import com.example.danie.dibapp.fragments.SearchFragment;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class SearchActivity extends AppCompatActivity
        implements SearchView.OnQueryTextListener, SearchFragment.OnFavoritePress,
        SearchFragment.getTeacherDataList, OnselectAction {

    private String[] url = {"http://dibbari.altervista.org/android_connect/get_all_docenti_studio.php"};
    private SearchView searchView;
    private TeacherDbHelper teacherDbHelper;
    public static List<TeacherData> teacherDataList;
    public static JsonArray arrayJson;

    //snackbar
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        //inizializza il loader
        LoadData load = new LoadData();

        //inizializza il db
        teacherDbHelper = new TeacherDbHelper(getApplicationContext());

        //prendi i dati Json in background
        load.execute(url);


    }

    /**
     * Quando l'utente rimuove o aggiunge il personale dai preferiti
     * @param name il nome.
     * @param isFavorite se e' preferito o no.
     * @param id id che individua univocamente il personale.
     */
    public void FavoriteAddOrRemove(String name, String surname, String ruolo,
                                    boolean isFavorite,String id, String urlPhoto, Bitmap s) {

        int valueBolToInt;
        if (isFavorite) {
            valueBolToInt = 0;
        }else {
            valueBolToInt = 1;
        }
        //visualizza il messaggio nella snackbar
        viewMessageOnSnackBar(name, surname, valueBolToInt);

        //cambia lo stato di isFavorite
        if (!isFavorite) {
            SQLiteDatabase db = teacherDbHelper.getWritableDatabase();
            String query = "DELETE FROM " +teacherDbHelper.TABLE_DOCENTE
                    +" WHERE "+teacherDbHelper.ID_D +"=?";
            try {
                db.execSQL(query, new String[]{id});

                //rimuovi l'immagine dalla cache
                this.deleteFile(id);
            }catch(Exception e) {
                e.printStackTrace();
            }finally {
                if (db != null) {
                    db.close();
                }
            }
        } else {

            SQLiteDatabase db = teacherDbHelper.getWritableDatabase();
            String[] str = {id, name, surname, "", ruolo, "", "", "1" , urlPhoto};

            String query = "INSERT OR REPLACE INTO " +teacherDbHelper.TABLE_DOCENTE +
                    " (" +teacherDbHelper.COLUMNS[0] +","+teacherDbHelper.COLUMNS[1]+","+teacherDbHelper.COLUMNS[2]
                    +"," +teacherDbHelper.COLUMNS[3] +"," +teacherDbHelper.COLUMNS[4]+"," +teacherDbHelper.COLUMNS[5]
                    +"," +teacherDbHelper.COLUMNS[6] +"," +teacherDbHelper.COLUMNS[7] +"," +teacherDbHelper.COLUMNS[8]+")"
                    +" VALUES (?,?,?,?,?,?,?,?,?)";
            //TODO: fare la query per salvare le informazioni totali nel DB per visualizzarli al click offline.

            //salva l'immagine per i preferiti (anche offline)

            String filename = id;
            FileOutputStream outputStream = null;
            try {
                outputStream = this.openFileOutput(filename, Context.MODE_PRIVATE);
                s.compress(Bitmap.CompressFormat.PNG,100,outputStream);
                outputStream.flush();
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (outputStream != null) {
                        outputStream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


            try {
                db.execSQL(query, str);
            }catch (Exception e) {
                Log.d("ERROR", "FavoriteAddOrRemove: "+e.getStackTrace());
            }finally {
                if (db != null) {
                    db.close();
                }
            }
        }
    }

    /**
     * Quando viene selezionato un elemento nella lista il metodo viene chiamato.
     * @param id ID che identifica univocamente il personale.
     */
    public void OnListenerItemSelection(String id) {
        Bundle bundle = new Bundle();
        bundle.putString("ID", id);
        fragChange(bundle, new ProfFragment());

    }

    public List<TeacherData> getList() {
        return teacherDataList;
    }

    @Override
    public void viewMessageOnSnackBar(String name, String surname, int status) {
        String str = null;
        switch (status) {
            case 0:
                str = " " +getString(R.string.add_preferite);
                break;
            case 1:
                str = " " +getString(R.string.rm_preferite);
                break;
            case 2:
                str = " " +getString(R.string.noConnection);
                break;
            case 3:
                str = " " +getString(R.string.messageInfoFirstStart);
                break;
        }

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                .coordinatorLayout);

        Snackbar snackbar = null;
        if (status == 2) {
            snackbar = Snackbar
                    .make(coordinatorLayout, str, Snackbar.LENGTH_LONG);
        }else if (status <= 1){
            snackbar = Snackbar
                    .make(coordinatorLayout, surname +" " +name +str, Snackbar.LENGTH_SHORT);
        }else {
            snackbar = Snackbar
                    .make(coordinatorLayout, str, Snackbar.LENGTH_LONG);
        }
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }

    public TeacherData getProfById(String id){
        Log.e("error", "Entrato in getProfByID");
        for(TeacherData item : teacherDataList){
           // if(item.getId() == id){ L muert d Daniel (2 ore e 15 minuti)
            if((item.getId()).equals(id)){
                return item;
            }
        }
        return null;
    }

    /**
     * carica i dati json dallo scriptPHP.
     */
    class LoadData extends AsyncTask<String,Void,String> {

        //la stringa contenente il Json
        public String value;
        ProgressDialog progDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progDialog = new ProgressDialog(SearchActivity.this);
            progDialog.setMessage("Caricamento");
            progDialog.setIndeterminate(false);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setCancelable(true);
            progDialog.show();
        }

        @Override
        protected String doInBackground(String... url) {
            getJson();
            setListFromJson();
            //TODO: aggiungere manualmente il cambio di fragment quando ha terminato le operazioni di dwl.
            return null;
        }

        private void getJson() {
            int time = 3000;
            String jsonString;
            final String nameArrayJson = "docente";
            JsonParser parser = new JsonParser();
            ServerConnection con = new ServerConnection();
            jsonString = con.getJsonString(url[0],time);
            if (jsonString != null && !(jsonString.equalsIgnoreCase(""))) {
                JsonElement element = parser.parse(jsonString);
                JsonObject objJson = element.getAsJsonObject();
                arrayJson = objJson.get(nameArrayJson).getAsJsonArray();    //TODO DOCENTI
                Log.e("tag :",(String.valueOf(arrayJson.size())));
            }
        }

        /**
         * Inizializza la lista del personale con i dati provenienti dal Json e controlla se ci sono elementi
         * che sono stati già salvati nel database (I preferiti sono salvati in modo persistente nel DB).
         */
        private void setListFromJson() {

            //array contenente i docenti
            JsonArray jsArray = arrayJson;
            teacherDataList = new ArrayList<>();
            Cursor c = null;
            SQLiteDatabase db = teacherDbHelper.getWritableDatabase();

            try {
                c = db.query(
                        teacherDbHelper.TABLE_DOCENTE,  // The table to query
                        null,                               // The COLUMNS to return
                        null,                                // The COLUMNS for the WHERE clause
                        null,                            // The values for the WHERE clause
                        null,                                     // don't group the rows
                        null,                                     // don't filter by row groups
                        null                                // The sort order
                );
            }catch(Exception e){
                Log.e("ERROR", "setListFromJson: ", e);
            }finally{
            }

            if (jsArray != null) {
                for (JsonElement jsonElem : jsArray) {
                    String id = jsonElem.getAsJsonObject().get(TeacherData.TAG_ID).getAsString();
                    String nome = jsonElem.getAsJsonObject().get(TeacherData.TAG_NOME).getAsString();
                    String cognome = jsonElem.getAsJsonObject().get(TeacherData.TAG_COGNOME).getAsString();
                    String ruolo = jsonElem.getAsJsonObject().get(TeacherData.TAG_RUOLO).getAsString();
                    String telefono = jsonElem.getAsJsonObject().get(TeacherData.TAG_TELEFONO).getAsString();
                    String mail = jsonElem.getAsJsonObject().get(TeacherData.TAG_MAIL).getAsString();
                    String sito = jsonElem.getAsJsonObject().get(TeacherData.TAG_SITO).getAsString();
                    String linkImg = jsonElem.getAsJsonObject().get(TeacherData.TAG_IMG).getAsString();
                    String stanza = jsonElem.getAsJsonObject().get(TeacherData.TAG_STANZA).getAsString();
                    String piano = jsonElem.getAsJsonObject().get(TeacherData.TAG_PIANO).getAsString();
                    //TODO: da modificare nel caso si vuole scaricare le info dei corsi (Da modificare anche TeacherData)
                    Boolean favorite = false;

                    c.moveToFirst();

                    Log.d("ERROR", String.valueOf(c.getCount()));
                    int columnPreferito = 7;

                    for (int i = 0; i < c.getCount(); i++) {
                        if (c.getCount() > 0 && c.getInt(columnPreferito) == 1) {
                            String idOfFavorite = c.getString(0);
                            if (id.equalsIgnoreCase(idOfFavorite)) {
                                favorite = true;
                                break;
                            }
                        }
                        c.moveToNext();
                    }
                    TeacherData td = new TeacherData(id, nome, cognome, ruolo, sito, linkImg, mail, telefono
                            , stanza, piano, favorite);
                    teacherDataList.add(td);

                }
            }
        }

        @Override
        protected void onPostExecute(String value) {
            super.onPostExecute(value);
            if (progDialog != null) {
                if (progDialog.isShowing()) {
                    progDialog.dismiss();
                }
                //avvia il fragment che conterrà la listView
                SearchFragment fragment = new SearchFragment();
                fragChange(null, fragment);
            }
        }
    }

    /**
     * Permette di sostituire il fragment dall'activity che lo opsita, puo' eventualmente veicolare
     * dati attraverso un bundle.
     * @param bundle dei dati, null se vuoto.
     * @param fragment il fragment sostitutivo.
     */
    public void fragChange(Bundle bundle,Fragment fragment) {

        if (bundle != null && bundle.getInt(FavoriteFragment.KEY_CHANGE_FRAGMENT) != 0) {
            LoadData load = new LoadData();
            load.execute();
            return;

        }
        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction().addToBackStack(null);
        fragment.setHasOptionsMenu(true);
        if (bundle != null) {
            fragment.setArguments(bundle);
        }

        fragmentTransaction.replace(R.id.fragContainer,fragment);
        fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option, menu);
        MenuItem searchItem = menu.findItem(R.id.search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        //imposta il listner sul ViewSearch
        searchView.setOnQueryTextListener(this);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, SearchActivity.class)));
        searchView.setIconifiedByDefault(false);
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);

            //se la ricerca non e' valida allora
            if (!TeacherProviderContent.isValidSearch()) {
                coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                        .coordinatorLayout);
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout, query +" " +getString(R.string.not_found), Snackbar.LENGTH_LONG);
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();

                //se la ricerca e' valida ma non completa (non un unico risultato)
            }else{

                //visualizza un messaggio, l'utente deve premere su una scelta per possibili omonimie
                coordinatorLayout = (CoordinatorLayout) findViewById(R.id
                        .coordinatorLayout);
                Snackbar snackbar = Snackbar
                        .make(coordinatorLayout," " +getString(R.string.info_search), Snackbar.LENGTH_LONG);
                View sbView = snackbar.getView();
                TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
                textView.setTextColor(Color.YELLOW);
                snackbar.show();
            }

        } else if (Intent.ACTION_VIEW.equals(intent.getAction())) {
            Uri uri = intent.getData();
            final String ID_SELEZIONATO = uri.getLastPathSegment();
            Bundle bundle = new Bundle();
            bundle.putString("ID", ID_SELEZIONATO);
            Log.e("error", ID_SELEZIONATO);
            //quindi carica da qui il nuovo fragment sfruttando id (ID_SELEZIONATO).
            fragChange(bundle, new ProfFragment());

            //startActivity(intent2);
            //quindi dovevo caricare da qui il nuovo fragment/ACTIVITY sfruttando id (ID_SELEZIONATO).
        }
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        //quando l'utente immette una stringa premendo invio (si è preferito usare un ContententProvider per la ricerca)
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        //quando l'utente immette un nuovo carattere (si è preferito usare un ContententProvider per la ricerca)
        return false;
    }


}

