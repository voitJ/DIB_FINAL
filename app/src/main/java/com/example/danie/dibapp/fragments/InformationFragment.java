package com.example.danie.dibapp.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.danie.dibapp.R;

public class InformationFragment extends Fragment {

    private TextView mTextView;

    private String[] stringsArray;
    private ListView mListView;
    private ArrayAdapter<String> mArrayAdapter;

    private WebView mWebView;

    private ProgressDialog progressDialog;

    private int courses;
    private final static String[] manifestiArray = {
            "http://www.uniba.it/ricerca/dipartimenti/informatica/didattica/corsi-di-laurea/informatica-270/regolamento-infomatica-2015-16",
            "http://www.uniba.it/ricerca/dipartimenti/informatica/didattica/corsi-di-laurea/informatica-tps-270/regolamento-infomatica-tps-2015-16",
            "http://www.uniba.it/ricerca/dipartimenti/informatica/didattica/corsi-di-laurea/informatica-icd-taranto-270/regolamento-infomatica-icd-taranto-2015-16",
            "http://www.uniba.it/ricerca/dipartimenti/informatica/didattica/corsi-di-laurea/informatica-magistrale/regolamento-infomatica-magistrale-2015-16"
    };
    private final static String[] lezioniArray = {
            "http://www.uniba.it/ricerca/dipartimenti/informatica/didattica/corsi-di-laurea/informatica-270/OrarioINFIIsemestre201516.pdf",
            "http://www.uniba.it/ricerca/dipartimenti/informatica/didattica/corsi-di-laurea/informatica-tps-270/OrarioITPSIIsem201516.pdf",
            "http://www.uniba.it/ricerca/dipartimenti/informatica/didattica/corsi-di-laurea/informatica-icd-taranto-270/OrarioICDTARANTOIIsem2016.pdf",
            "http://www.uniba.it/ricerca/dipartimenti/informatica/didattica/corsi-di-laurea/informatica-magistrale/OrarioMagistraleIIsem201516.pdf"
    };
    private final static String[] laboratorioArray = {
            "http://www.uniba.it/ricerca/dipartimenti/informatica/didattica/corsi-di-laurea/informatica-270/orario-laboratori",
            "http://www.uniba.it/ricerca/dipartimenti/informatica/didattica/corsi-di-laurea/informatica-270/orario-laboratori",
            "",
            "http://www.uniba.it/ricerca/dipartimenti/informatica/didattica/corsi-di-laurea/informatica-270/orario-laboratori"
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        stringsArray = getResources().getStringArray(R.array.courses_list);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_list, stringsArray);

        return inflater.inflate(R.layout.fragment_information,container,false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTextView = (TextView) getActivity().findViewById(R.id.information_textView);
        mTextView.setText("Corsi di laurea");
        mListView = (ListView) view.findViewById(R.id.information_listView);
        setListView(mListView);
        mWebView = (WebView) view.findViewById(R.id.information_webView);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setVisibility(View.INVISIBLE);
    }

    private void setListView(final ListView listView){
        listView.setAdapter(mArrayAdapter);
        listView.setClickable(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,int position, long id) {
                switch (stringsArray[position]){
                    case "...":
                        courses = -1;
                        mTextView.setText("Corsi di laurea");
                        stringsArray = getResources().getStringArray(R.array.courses_list);
                        mArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_list, stringsArray);
                        listView.setAdapter(mArrayAdapter);
                        break;
                    case "Informatica":
                        courses = 0;
                        mTextView.setText("Servizi");
                        stringsArray = getResources().getStringArray(R.array.services_list);
                        mArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_list, stringsArray);
                        listView.setAdapter(mArrayAdapter);
                        break;
                    case "Informatica e Tecnologie per la Produzione del Softwere":
                        courses = 1;
                        mTextView.setText("Servizi");
                        stringsArray = getResources().getStringArray(R.array.services_list);
                        mArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_list, stringsArray);
                        listView.setAdapter(mArrayAdapter);
                        break;
                    case "Informatica e Comunicazione Digitale":
                        courses = 2;
                        mTextView.setText("Servizi");
                        stringsArray = getResources().getStringArray(R.array.services_list);
                        mArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_list, stringsArray);
                        listView.setAdapter(mArrayAdapter);
                        break;
                    case "Informatica Magistrale":
                        courses = 3;
                        mTextView.setText("Servizi");
                        stringsArray = getResources().getStringArray(R.array.services_list);
                        mArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_list, stringsArray);
                        listView.setAdapter(mArrayAdapter);
                        break;
                    case "Manifesto degli Studi":
                        mTextView.setText("Manifesto");
                        mListView.setVisibility(View.INVISIBLE);
                        mWebView.setVisibility(View.VISIBLE);
                        mWebView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + manifestiArray[courses]);
                        break;
                    case "Calendario Lezioni":
                        mTextView.setText("Orario Lezioni");
                        mListView.setVisibility(View.INVISIBLE);
                        mWebView.setVisibility(View.VISIBLE);
                        mWebView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + lezioniArray[courses]);
                        break;
                    case "Orario Laboratori":
                        if(courses == 2){
                            Toast.makeText(getActivity(), "Orari del laboratorio non presenti", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            mTextView.setText("Orario Laboratori");
                            mListView.setVisibility(View.INVISIBLE);
                            mWebView.setVisibility(View.VISIBLE);
                            mWebView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + laboratorioArray[courses]);
                        }
                        break;
                    case "Insegnamenti, docenti e programmi":
                        break;
                    case "Sedute di Laurea":
                        break;
                }
            }
        });
    }
}
