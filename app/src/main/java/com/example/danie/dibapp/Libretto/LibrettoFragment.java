package com.example.danie.dibapp.Libretto;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.danie.dibapp.Data.DividerItemDecoration;
import com.example.danie.dibapp.MainActivity;
import com.example.danie.dibapp.R;
import com.example.danie.dibapp.fragments.dummy.DummyContent.DummyItem;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class LibrettoFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private static FloatingActionButton fab;
    private String sortBy = null;
    private SharedPreferences settings;
    private static final String PREFS_KEY = "orderType";
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    static FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mDatabaseReference;
    private String userId;
    ArrayList<Exam> examArrayList = new ArrayList<Exam>();
    private static final String SHARED_PREFERENCES_TYPE = "DASHBOARD";
    TextView mEmptyView;
    FirebaseRecyclerAdapter<Exam, ExamViewHolder> adapter;
    DateFormat dbFormat = new SimpleDateFormat("yyyy/MM/dd");
    DateFormat recyclerViewFormat = new SimpleDateFormat("dd/MM/yyyy");

    public LibrettoFragment() {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MainActivity) getActivity())
                .setActionBarTitle("Libretto");
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static LibrettoFragment newInstance(int columnCount) {
        LibrettoFragment fragment = new LibrettoFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        database.setPersistenceEnabled(true);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_libretto, container, false);
        setHasOptionsMenu(true);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        mEmptyView = (TextView) view.findViewById(R.id.empty);


        settings = getContext().getSharedPreferences(SHARED_PREFERENCES_TYPE, Context.MODE_PRIVATE);
        String sortBy = settings.getString(PREFS_KEY, null);
        if (sortBy == null) sortBy = "date";
        fillRecyclerView();

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                mLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);


        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), mRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {


            @Override
            public void onItemClick(View view, int position) {

                View w = mRecyclerView.getLayoutManager().findViewByPosition(position);
                TextView name = (TextView) w.findViewById(R.id.examName);
                TextView date = (TextView) w.findViewById(R.id.examDate);
                TextView grade = (TextView) w.findViewById(R.id.examGrade);
                TextView credits = (TextView) w.findViewById(R.id.examCredits);
                TextView professor = (TextView) w.findViewById(R.id.examProfessor);
                TextView notes = (TextView) w.findViewById(R.id.examNotes);
                Intent intent = new Intent(view.getContext(), ExamDetailsActivity.class);
                intent.putExtra("name", name.getText().toString());
                intent.putExtra("date", date.getText().toString());
                intent.putExtra("grade", grade.getText().toString());
                intent.putExtra("credits", credits.getText().toString());
                intent.putExtra("professor", professor.getText().toString());
                intent.putExtra("notes", notes.getText().toString());
                startActivity(intent);


            }


            public void onItemLongClick(View view, int pos) {


                View w = mRecyclerView.getLayoutManager().findViewByPosition(pos);
                TextView name = (TextView) w.findViewById(R.id.examName);
                TextView date = (TextView) w.findViewById(R.id.examDate);
                TextView grade = (TextView) w.findViewById(R.id.examGrade);
                TextView credits = (TextView) w.findViewById(R.id.examCredits);
                TextView professor = (TextView) w.findViewById(R.id.examProfessor);
                TextView notes = (TextView) w.findViewById(R.id.examNotes);
                final String nameToEdit = name.getText().toString();
                final String dateToEdit = date.getText().toString();
                final String gradeToEdit = grade.getText().toString();
                final String creditsToEdit = credits.getText().toString();
                final String professorToEdit = professor.getText().toString();
                final String notesToEdit = notes.getText().toString();

                final int position = pos;
                View v = getActivity().getLayoutInflater().inflate(R.layout.bottom_sheet, null);
                TextView editTextView = (TextView) v.findViewById(R.id.txt_modifica);
                TextView deleteTextView = (TextView) v.findViewById(R.id.txt_bottom_vote);


                final Dialog mBottomSheetDialog = new Dialog(getActivity(),
                        R.style.MaterialDialogSheet);
                mBottomSheetDialog.setContentView(v);
                mBottomSheetDialog.setCancelable(true);
                mBottomSheetDialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                mBottomSheetDialog.getWindow().setGravity(Gravity.BOTTOM);
                mBottomSheetDialog.show();


                editTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        String key = adapter.getRef(position).getKey();
                        Intent intent = new Intent(v.getContext(), EditExamActivity.class);
                        intent.putExtra("name", nameToEdit);
                        intent.putExtra("grade", gradeToEdit);

                        intent.putExtra("credits", creditsToEdit);
                        // 04/01/2017
                        intent.putExtra("date", dateToEdit);
                        intent.putExtra("professor", professorToEdit);
                        intent.putExtra("notes", notesToEdit);
                        intent.putExtra("key", key);
                        startActivityForResult(intent, 1);
                        mBottomSheetDialog.dismiss();
                    }
                });

                deleteTextView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {


                        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
                        adb.setTitle(R.string.delete);
                        adb.setMessage(R.string.delete_confirmation);
                        adb.setNegativeButton(R.string.cancel, null);
                        adb.setPositiveButton(R.string.ok, new AlertDialog.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                adapter.getRef(position).removeValue();


                            }
                        });
                        adb.show();
                        mBottomSheetDialog.dismiss();
                    }
                });


            }
        }));


        fab = (FloatingActionButton) view.findViewById(R.id.add_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Intent intent = new Intent(getActivity(), AddExamActivity.class);
                startActivity(intent);

            }
        });


        examArrayList = new ArrayList<Exam>();
        mDatabaseReference = database.getReference();


        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users").child(userId).child("exams");
        mDatabaseReference.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        for (DataSnapshot examSnapshot : dataSnapshot.getChildren()) {

                            Exam exam = examSnapshot.getValue(Exam.class);
                            examArrayList.add(exam);
                            Log.e("Chat", "mDatabasereference: " + examArrayList.size());


                        }

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


        settings = getContext().getSharedPreferences(SHARED_PREFERENCES_TYPE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(PREFS_KEY, sortBy);
        editor.commit();

        return view;
    }


    public void fillRecyclerView() {


        String sortBy = settings.getString(PREFS_KEY, null);
        if (sortBy == null) sortBy = "date";
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();
        mDatabaseReference = database.getReference();
        if (mRecyclerView != null) {

            mRecyclerView.setHasFixedSize(true);
        }

        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dy > 0)
                    fab.hide();
                else if (dy < 0)
                    fab.show();
            }


        });


        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);


        /*DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                mLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);*/
        adapter = new FirebaseRecyclerAdapter<Exam, ExamViewHolder>(


                Exam.class,
                R.layout.exam_item,
                ExamViewHolder.class,
                mDatabaseReference.child("users").child(userId).child("exams").getRef().orderByChild(sortBy)
        )

        {
            @Override
            protected void populateViewHolder(final ExamViewHolder viewHolder, final Exam model, int position) {
                if (mEmptyView.getVisibility() == View.VISIBLE) {
                    mEmptyView.setVisibility(View.GONE);
                }
                viewHolder.name.setText(String.valueOf(model.getName()));
                int grade = model.getGrade();
                if (grade == 17) viewHolder.grade.setText(" ID");
                else if (grade == 31) viewHolder.grade.setText(" L");
                else viewHolder.grade.setText(String.valueOf(model.getGrade()));
                viewHolder.credits.setText(String.valueOf(model.getCredits()) + " CFU");

                String dbDateStr = String.valueOf(model.getDate());
                Date date = null;
                try {
                    date = dbFormat.parse(dbDateStr);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String recyclerViewDateStr = recyclerViewFormat.format(date);
                viewHolder.date.setText(recyclerViewDateStr);
                viewHolder.professor.setText(String.valueOf(model.getProfessor()));
                viewHolder.notes.setText(String.valueOf(model.getNotes()));

            }
        };


        mRecyclerView.setAdapter(adapter);


    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 1) {

            String key = data.getStringExtra("key");
            mDatabaseReference.child(key).removeValue();


        } else {
        }


    }


    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name


        void onListFragmentInteraction(DummyItem item);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.list_sort, menu);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        final CharSequence[] items = {"Data", "Nome"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Scegli l'ordine di visualizzazione");
        builder.setSingleChoiceItems(items, -1, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {


                switch (item) {
                    case 0:
                        sortBy = "date";
                        break;
                    case 1:
                        sortBy = "name";
                        break;


                }

            }
        });

        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                settings = getContext().getSharedPreferences(SHARED_PREFERENCES_TYPE, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(PREFS_KEY, sortBy);
                editor.commit();
                fillRecyclerView();


            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });


        AlertDialog ad = builder.create();
        ad.show();


        return true;

    }


    public static class ExamViewHolder extends RecyclerView.ViewHolder {


        TextView name;
        TextView grade;
        TextView credits;
        TextView date;
        TextView professor;
        TextView notes;
        View mView;

        public ExamViewHolder(View v) {
            super(v);
            mView = itemView;
            name = (TextView) v.findViewById(R.id.examName);
            grade = (TextView) v.findViewById(R.id.examGrade);
            credits = (TextView) v.findViewById(R.id.examCredits);
            date = (TextView) v.findViewById(R.id.examDate);
            professor = (TextView) v.findViewById(R.id.examProfessor);
            notes = (TextView) v.findViewById(R.id.examNotes);
        }


    }


}