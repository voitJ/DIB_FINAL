package com.example.danie.dibapp.fragments;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.danie.dibapp.Data.OnselectAction;
import com.example.danie.dibapp.Data.TeacherData;
import com.example.danie.dibapp.SearchActivity;
import com.example.danie.dibapp.R;
import com.example.danie.dibapp.database.TeacherDbHelper;

import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Michele on 14/12/2016.
 */

public class ProfFragment extends Fragment {

    TeacherDbHelper teacherDbHelper;
    private OnselectAction mcallBack;
    public String id;
    private ImageView image;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final String VALUE = "value";
        Bundle bundle = this.getArguments();
        Serializable mcallBack = null;

        id = this.getArguments().getString("ID");
        Log.d("prova", id);

        if (bundle != null) {
            mcallBack = bundle.getSerializable(VALUE);
        }
        if (mcallBack != null) {
            this.mcallBack = ((wrapInterface) mcallBack).getInterfaceCallBack();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return false;
    }

    public void onBackPressed() {
        ((SearchActivity) getActivity()).fragChange(null, new SearchFragment());
    }

    public class wrapInterface implements Serializable {
        OnselectAction interfaceCallBack;

        public wrapInterface(OnselectAction intfce) {
            this.interfaceCallBack = intfce;
        }

        public OnselectAction getInterfaceCallBack() {
            return interfaceCallBack;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        TeacherData prof = ((SearchActivity) getActivity()).getProfById(id);
        Log.d("prova2", id);
        // Inflate layout
        View root = inflater.inflate(R.layout.fragment_prof, container, false);

        TextView nomeD = (TextView) root.findViewById(R.id.pcognome);
        nomeD.setText(prof.getNome());
        TextView cognomeD = (TextView) root.findViewById(R.id.pnome);
        cognomeD.setText(prof.getCognome());
        TextView ruoloD = (TextView) root.findViewById(R.id.pruolo);
        ruoloD.setText(prof.getRuolo());
        TextView telefonoD = (TextView) root.findViewById(R.id.ptelefono);
        telefonoD.setText(String.valueOf(prof.getTelefono()));
        telefonoD.setClickable(true);
        TextView mailD = (TextView) root.findViewById(R.id.pmail);
        mailD.setText(prof.getMail());
        TextView sitoD = (TextView) root.findViewById(R.id.psito);
        sitoD.setText(prof.getSito());
        TextView stanzaD = (TextView) root.findViewById(R.id.pstanza);
        stanzaD.setText(prof.getStanza());
        TextView pianoD = (TextView) root.findViewById(R.id.ppiano);
        pianoD.setText(prof.getPiano());


        image = (ImageView) root.findViewById(R.id.pimage);
        DownloadFilesTask task = new DownloadFilesTask();
        task.execute(prof.getImgUrl());


        return root;
    }

    class DownloadFilesTask extends AsyncTask<String, Void, Bitmap> {

        public DownloadFilesTask() {
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return downImage(params[0], 1000);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Bitmap s) {
            super.onPostExecute(s);
            image.setImageBitmap(s);

        }

        private Bitmap downImage(String urlStr, int timeOutRequest) {
            HttpURLConnection urlConnection = null;
            Bitmap image = null;
            try {
                final String methodConnection = "SET";
                final URL url = new URL(urlStr);

                urlConnection = (HttpURLConnection) url.openConnection();

                // imposta la connessione
                //urlConnection.setRequestMethod(methodConnection);
                // urlConnection.setRequestProperty("Content-length", "0");
                //urlConnection.setUseCaches(false);
                //urlConnection.setAllowUserInteraction(false);
                //urlConnection.setConnectTimeout(1000);
                //urlConnection.setReadTimeout(timeOutRequest);

                // apre la connessione
                urlConnection.connect();

                // stato connessione http
                int status = urlConnection.getResponseCode();

                switch (status) {
                    case 200:
                    case 201:
                        InputStream stream = urlConnection.getInputStream();
                        image = BitmapFactory.decodeStream(stream);
                        Log.e("ERR",String.valueOf(image.getHeight()));
                        break;

                    default:
                        //null
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return image;
        }





        private Bitmap getRoundedShape(Bitmap scaleBitmapImage) { //TODO: PER ICONE CIRCOLARI
            int targetWidth = 86;
            int targetHeight = 86;
            Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                    targetHeight,Bitmap.Config.ARGB_8888);

            Canvas canvas = new Canvas(targetBitmap);
            Path path = new Path();
            path.addCircle(((float) targetWidth - 1) / 2,
                    ((float) targetHeight - 1) / 2,
                    (Math.min(((float) targetWidth),
                            ((float) targetHeight)) / 2),
                    Path.Direction.CCW);

            canvas.clipPath(path);
            canvas.drawBitmap(scaleBitmapImage,
                    new Rect(0, 0, scaleBitmapImage.getWidth(),
                            scaleBitmapImage.getHeight()),
                    new Rect(0, 0, targetWidth, targetHeight), null);

            return targetBitmap;
        }
    }
}

