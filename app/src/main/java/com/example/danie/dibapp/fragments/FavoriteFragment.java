package com.example.danie.dibapp.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.danie.dibapp.Data.ListTeacherAdapter;
import com.example.danie.dibapp.R;
import com.example.danie.dibapp.SearchActivity;
import com.example.danie.dibapp.database.TeacherDbHelper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import com.example.danie.dibapp.Data.OnselectAction;


/**
 * Fragment che contiene la lista dei Favoriti.
 */
public class FavoriteFragment extends Fragment {

    private TeacherDbHelper teacherDbHelper;
    private ListView list;
    private MyCursorAdapter adapter;
    private Cursor cursor;
    private OnselectAction aCallBack;
    private SearchFragment.OnFavoritePress mCallBack;
    private int countSelection = 0;
    public static final int CHANGE_TO_SEARCH_FRAGMENT = 2;
    public static final String KEY_CHANGE_FRAGMENT = "typeFragment";
    private static final String KEY_INTRO_APP_INFORMATION = "keyValue0";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //inizializza il db
        try {
            teacherDbHelper = new TeacherDbHelper(getContext());
        }catch (Exception e) {
            Log.d("ERROR", "onCreate DB: " +e.getStackTrace());
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        //Avviso da visualizzare solo una volta sul salvataggio offline dei dati.
        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        if (sharedPref.getInt(KEY_INTRO_APP_INFORMATION, 0) == 0) {
            int messageViewed = 1;
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt(KEY_INTRO_APP_INFORMATION, messageViewed);
            editor.apply();
            mCallBack.viewMessageOnSnackBar(null, null, 3);

            }//else il messaggio e' stato gia' visualizzato
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (teacherDbHelper != null) {
            teacherDbHelper.close();
        }
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            aCallBack = (OnselectAction) activity;
            mCallBack = (SearchFragment.OnFavoritePress) activity;


        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFavoritePress");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_favorite, container, false);
        list = (ListView) root.findViewById(R.id.listView_favorite);
        cursor = initializeCursorAdapter();
        adapter = new MyCursorAdapter(getContext(),cursor,0);
        list.setAdapter(adapter);


        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //al click della lista nei preferiti, ID e' la variabile che chiama ProfFragment relativo
                final String ID_SELEZIONATO = Long.toString(id);
                Bundle bundle = new Bundle();
                bundle.putString("ID", ID_SELEZIONATO);
                ((SearchActivity) getActivity()).fragChange(bundle, new ProfFragment());

                //prova da cancellare per michele
                Toast.makeText(getActivity(), "CLICK "+id, Toast.LENGTH_SHORT).show();
            }
        });

        list.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        list.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

            /**
             * Quando si seleziona un elemento.
             */
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position,
                                                  long id, boolean checked) {
                View view = list.getChildAt(position - list.getFirstVisiblePosition());
                if (adapter.checkID(position)) {
                    adapter.unsetSelection(id);
                    countSelection--;
                    mode.setTitle(getString(R.string.selezione) +" " +countSelection);
                    if (view != null) {
                        view.setBackgroundColor
                                (getResources().getColor(android.R.color.background_light));
                        int[] attrs = new int[]{R.attr.selectableItemBackground};
                        TypedArray typedArray = getActivity().obtainStyledAttributes(attrs);
                        int backgroundResource = typedArray.getResourceId(0, 0);
                        view.setBackgroundResource(backgroundResource);
                    }

                }else {
                    countSelection++;
                    adapter.setSelection(position, id);
                    mode.setTitle(getString(R.string.selezione) +" " +countSelection);
                    if (list.getChildAt(position  - list.getFirstVisiblePosition()) != null) {
                        MyCursorAdapter.Holder holder = adapter.getHolder();
                        adapter.mapColorBackground.remove(position);
                        list.getChildAt(position  - list.getFirstVisiblePosition()).setBackgroundColor
                                (getResources().getColor(R.color.actionMode));
                        holder.setBlu(position,true);
                        view.setTag(holder);
                    }
                }
            }
            /**
             * Quando si preme a lungo su un dato.
             */
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = getActivity().getMenuInflater();
                inflater.inflate(R.menu.contextual_menu_delete, menu);
                //Toast.makeText(getContext(), "prova 1", Toast.LENGTH_SHORT).show();
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                //Toast.makeText(getContext(), "prova 2", Toast.LENGTH_SHORT).show();
                return false;
            }

            /**
             * Quando si preme sull'action item (cestino).
             */
            @Override
            public boolean onActionItemClicked(final ActionMode mode,final MenuItem item) {

                Toast.makeText(getContext(), "prova 3", Toast.LENGTH_SHORT).show();
                final AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle(getString(R.string.allertEliminazione));

                alertDialog.setNegativeButton("annulla", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });


                alertDialog.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        adapter.reloadAdapter();
                        mode.finish();
                    }
                });
                alertDialog.show();
                return false;
            }

            /**
             * Si annulla l'azione.
             */
            @Override
            public void onDestroyActionMode(ActionMode mode) {

                /*Set<Integer> keySet = adapter.viewMap.keySet();

                for (int key : keySet) {
                    adapter.viewMap.get(key).setBackgroundColor
                            (ContextCompat.getColor(getContext(), android.R.color.background_light));
                }*/
                countSelection = 0;
                adapter.map.clear();
                adapter.mapColorBackground.clear();
            }
        });

        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        //imposta il listener sullo spinner
        MenuItem item = menu.findItem(R.id.menu_spinner_favorite);
        Spinner spinner = (Spinner) MenuItemCompat.getActionView(item);
        setspinnerListener(spinner);
    }

    private void setspinnerListener(Spinner spinner) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {

                    //passa a visulizzare tutti cambiando la struttura collegata all'adapter
                    SearchFragment fragment = new SearchFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt(KEY_CHANGE_FRAGMENT,CHANGE_TO_SEARCH_FRAGMENT);
                    ((SearchActivity) getActivity()).fragChange(bundle,fragment);
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }


    private Cursor initializeCursorAdapter() {
        SQLiteDatabase db = null;
        try {
            db = teacherDbHelper.getWritableDatabase();
            cursor = db.query(
                    teacherDbHelper.TABLE_DOCENTE,  // The table to query
                    null,                        // The COLUMNS to return
                    null,                        // The COLUMNS for the WHERE clause
                    null,                        // The values for the WHERE clause
                    null,                        // don't group the rows
                    null,                        // don't filter by row groups
                    null                         // The sort order
            );
        }catch (SQLiteException e) {
            Log.e("ERROR", "initializeDataAdapter: ", e);
        }finally {

        }
        return cursor;

    }


    class MyCursorAdapter extends CursorAdapter {

        private LayoutInflater mLayoutInflater;
        HashMap<Integer,Long> map = new HashMap<>();
        private Cursor cursor;
        final HashMap<Integer,View> viewMap = new HashMap<>();
        final HashMap<Integer,Boolean> mapColorBackground = new HashMap<>();




        public MyCursorAdapter(Context context, Cursor cursor, int flags) {
            super(context,cursor,flags);
            mLayoutInflater = LayoutInflater.from(context);
            this.cursor = cursor;
        }

        /**
         * rimuove l'oggetto selezionato dal dizionario.
         * @param position la posizione della risorsa.
         */
        public void setSelection(int position,long id) {
            map.put(position,id);
        }

        /**
         * rimuove l'oggetto deselezionato dal dizionario.
         * @param position la posizione della risorsa.
         */
        public void unsetSelection(long position) {
            map.remove(position);
        }

        /**
         * controlla se è selezionato.
         * @param position la posizione della risorsa.
         * @return vero se e' selezionato, falso altrimenti.
         */
        public boolean checkID(int position) {
            boolean check;
            if (map.get(position) != null) {
                check = true;
            }else {
                check = false;
            }
            return check;
        }

        public void reloadAdapter() {
            Set<Integer> keySet = map.keySet();
            for (int key : keySet) {
                String id = String.valueOf(map.get(key));
                aCallBack.FavoriteAddOrRemove("","","",false,id,"",null);
            }
            adapter.swapCursor(initializeCursorAdapter());
            adapter.mapColorBackground.clear();
            adapter.notifyDataSetChanged();
            list.setAdapter(adapter);
            countSelection = 0;
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View  v = mLayoutInflater.inflate(R.layout.personale_row_copy, parent, false);
            return v;
        }

        /*@Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //if (convertView != null) {
               // Log.i("position getView :", String.valueOf(position) + " " + convertView);
            //}
            *//*if (map.get(position) != null) {
                v.setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.holo_blue_light));
            }else {

                v.setBackgroundColor(ContextCompat.getColor(getContext(), android.R.color.background_light));
                int[] attrs = new int[]{R.attr.selectableItemBackground};
                TypedArray typedArray = getActivity().obtainStyledAttributes(attrs);
                int backgroundResource = typedArray.getResourceId(0, 0);
                v.setBackgroundResource(backgroundResource);
            }*//*
                return convertView;
        }
*/

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            if (convertView != null) {
                //TODO: POSSIBILE OTTIMIZZAZIONE, VIENE RIPRISTINATO SEMPRE IL COLORE BACKGROUND
                /**
                 * POSSIBILE SOLUZIONE: UTILIZZARE I TAG PER CONTROLLARE NEL RAMO ELSE SE
                 * QUEL ROW HA COLORE O MENO (DA TESTARE).
                 */

                Holder holder = (Holder) convertView.getTag();
                Boolean color = null;

                if (color = mapColorBackground.get(position) != null) {
                    convertView.setBackgroundColor
                            (getResources().getColor(R.color.actionMode));
                } else {
                    convertView.setBackgroundColor
                            (getResources().getColor(android.R.color.background_light));
                    int[] attrs = new int[]{R.attr.selectableItemBackground};
                    TypedArray typedArray = getActivity().obtainStyledAttributes(attrs);
                    int backgroundResource = typedArray.getResourceId(0, 0);
                    convertView.setBackgroundResource(backgroundResource);
                }
            }
            Log.i("position getView :", String.valueOf(position) + " " + convertView);
            return super.getView(position, convertView, parent);
        }

        @Override
        public void bindView(View view, Context context, final Cursor cursor) {

            final String nome = cursor.getString(cursor.getColumnIndexOrThrow(TeacherDbHelper.NOME_D));
            final String cognome = cursor.getString(cursor.getColumnIndexOrThrow(TeacherDbHelper.COGNOME_D));
            final String ruolo = cursor.getString(cursor.getColumnIndexOrThrow(TeacherDbHelper.RUOLO_D));
            final String id = cursor.getString(cursor.getColumnIndexOrThrow(TeacherDbHelper.ID_D));

            TextView impiegato = (TextView) view.findViewById(R.id.docente);
            impiegato.setText(cognome +" " +nome);
            TextView tipoImpiegato = (TextView) view.findViewById(R.id.tipo);
            tipoImpiegato.setText(ruolo);
            ImageView imageImpiegato = (ImageView) view.findViewById(R.id.image8);


            FileInputStream inputStream = null;
            Bitmap imageCached;
            try {
                inputStream = context.openFileInput(id);
                imageCached = BitmapFactory.decodeStream(inputStream);
                imageImpiegato.setImageBitmap(imageCached);
            } catch (FileNotFoundException e) {
                //carica l'immagine di default
                Bitmap iconDefaultInRow = BitmapFactory.decodeResource(context.getResources(),R.drawable.profile_icon);
                iconDefaultInRow = ListTeacherAdapter.getRoundedShape(iconDefaultInRow);
                imageImpiegato.setImageBitmap(iconDefaultInRow);
                e.printStackTrace();
            }finally {
                try {
                    if (inputStream != null) {
                        inputStream.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


                //Log.e("posizione", String.valueOf(cursor.getPosition()));
            Log.e("bind cod " +String.valueOf(cursor.getPosition()), String.valueOf(view));
        }


        /*LikeButton star = (LikeButton) view.findViewById(R.id.isFavorite);
            star.setLiked(tipoBol);

            star.setOnLikeListener(new OnLikeListener() {
                @Override
                public void liked(LikeButton likeButton) {
                    mcallBack.FavoriteAddOrRemove(nome,true,id);
                }

                @Override
                public void unLiked(LikeButton likeButton) {
                    mcallBack.FavoriteAddOrRemove(nome,false,id);
                    cursor.close();
                    adapter.swapCursor(initializeDataAdapter());
                    adapter.notifyDataSetChanged();
                    list.setAdapter(adapter);

                }
            });*/


        class Holder {
            public void setBlu(int position, boolean colorBluBackground) {
                mapColorBackground.put(position, colorBluBackground);
            }
        }

        public Holder getHolder() {
            return new Holder();
        }
    }
}
