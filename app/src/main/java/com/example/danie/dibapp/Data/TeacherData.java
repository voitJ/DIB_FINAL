package com.example.danie.dibapp.Data;

import android.graphics.Bitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * Modella l'oggetto docente del Json.
 */
public class TeacherData {

    private String id;
    private String nome;
    private String cognome;
    private String ruolo;
    private String telefono;
    private String mail;
    private String sito;
    private String imgUrl;
    private String stanza;
    private String piano;
    private Boolean favorito;
    private Bitmap img;

    public static final String TAG_ID = "Id";
    public static final String TAG_NOME = "Nome";
    public static final String TAG_COGNOME = "Cognome";
    public static final String TAG_RUOLO = "Ruolo";
    public static final String TAG_TELEFONO = "Telefono";
    public static final String TAG_MAIL = "Mail";
    public static final String TAG_SITO = "Sito";
    public static final String TAG_IMG = "imgUrl";
    public static final String TAG_STANZA = "Stanza";
    public static final String TAG_PIANO = "Piano";

    public TeacherData(String id,String nome, String cognome,String ruolo, String sito,
                       String imgUrl, String mail, String telefono, String stanza, String piano,
                       Boolean favorito) {
        this.id = id;
        this.nome = nome;
        this.cognome = cognome;
        this.ruolo = ruolo;
        this.sito = sito;
        this.imgUrl = imgUrl;
        this.mail = mail;
        this.telefono = telefono;
        this.stanza = stanza;
        this.piano = piano;
        this.favorito = favorito;
    }

    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    public Long getTelefono() {
        return Long.parseLong(telefono);
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getSito() {
        return sito;
    }

    public void setSito(String sito) {
        this.sito = sito;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getStanza() {
        return stanza;
    }

    public void setStanza(String stanza) {
        this.stanza = stanza;
    }

    public String getPiano() {
        return piano;
    }

    public void setPiano(String piano) {
        this.piano = piano;
    }

    public Boolean getFavorito() {
        return favorito;
    }

    public void setFavorito(Boolean favorito) {
        this.favorito = favorito;
    }
}
