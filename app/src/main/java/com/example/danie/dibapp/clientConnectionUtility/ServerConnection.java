package com.example.danie.dibapp.clientConnectionUtility;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class ServerConnection {

    /**
     * Permette di collegarsi con protocollo HTTP con connessione GET.
     * @param urlStr la stringa del file php contenente il payload (ovvero il file json)
     *               Sintassi: "linkPHP"?str=laStringaConteneteIlJson
     * @param timeOutRequest tempo di connessione espresso im mls 3000mls = 3sec
     * @return la stringa Json di risposta.
     */
    public String getJsonString(String urlStr, int timeOutRequest) {
        HttpURLConnection urlConnection = null;
        String value = "";
        try {
            final String methodConnection = "GET";
            final URL url = new URL(urlStr);

            urlConnection = (HttpURLConnection) url.openConnection();

            // imposta la connessione
            urlConnection.setRequestMethod(methodConnection);
            urlConnection.setRequestProperty("Content-length", "0");
            urlConnection.setUseCaches(false);
            urlConnection.setAllowUserInteraction(false);
            urlConnection.setConnectTimeout(1000);
            urlConnection.setReadTimeout(timeOutRequest);

            // apre la connessione
            urlConnection.connect();

            // stato connessione http
            int status = urlConnection.getResponseCode();

            switch (status) {
                case 200:
                case 201:
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(urlConnection.getInputStream()));
                    String str = "";
                    StringBuilder sb = new StringBuilder();
                    while ((str = reader.readLine()) != null) {
                        sb.append(str);
                    }
                    value = sb.toString();
                    break;
                default:
                    value = null;
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return value;
    }
}
