package com.example.danie.dibapp.Data;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.danie.dibapp.R;
import com.example.danie.dibapp.database.TeacherDbHelper;
import com.like.LikeButton;
import com.like.OnLikeListener;

import org.apache.http.client.methods.HttpGet;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class ListTeacherAdapter extends RecyclerView.Adapter<ListTeacherAdapter.MyViewHolder> {

    private List<TeacherData> teacherList;
    OnselectAction onSelectAction;
    Context context;
    private Bitmap iconDefaultInRow;

    public  ListTeacherAdapter(List<TeacherData> teachersList, OnselectAction favorite,
                               Context context) {
        this.teacherList = teachersList;
        this.onSelectAction = favorite;
        this.context = context;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.personale_row, parent, false);
        ImageView imgView = (ImageView) itemView.findViewById(R.id.image8);

        //setta icona di default circolare per la lista
        iconDefaultInRow = BitmapFactory.decodeResource(context.getResources(),R.drawable.profile_icon);
        iconDefaultInRow = getRoundedShape(iconDefaultInRow);
        imgView.setImageBitmap(iconDefaultInRow);
        return new MyViewHolder(itemView);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView docente, tipo;
        private LikeButton star;
        private ImageView image;
        View view;

        public MyViewHolder(View view) {
            super(view);
            docente = (TextView) view.findViewById(R.id.docente);
            tipo = (TextView) view.findViewById(R.id.tipo);
            star = (LikeButton) view.findViewById(R.id.isFavorite);
            image = (ImageView) view.findViewById(R.id.image8);

            this.view = (ViewGroup) view;
        }
    }


    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final String NULL_STRING = "niente";
        final TeacherData teacherData = teacherList.get(position);
        holder.docente.setText(teacherData.getCognome() +" " +teacherData.getNome());
        holder.tipo.setText(teacherData.getRuolo());
        holder.star.setLiked(teacherData.getFavorito());
        //holder.image.setImageBitmap(BitmapFactory.decodeResource(context.getResources(),R.drawable.background_nav_bar));
        if (!teacherData.getImgUrl().equalsIgnoreCase("")) {
            DownloadFilesTask task = new DownloadFilesTask(holder, teacherData);
            task.execute(teacherData.getImgUrl());
        }else {
            teacherData.setImg(iconDefaultInRow);
        }
        final LikeButton star = holder.star;
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSelectAction.OnListenerItemSelection(teacherData.getId());
            }
        });

        star.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                onSelectAction.FavoriteAddOrRemove(teacherData.getNome(), teacherData.getCognome(),
                        teacherData.getRuolo(), true, teacherData.getId(), teacherData.getSito(),
                        teacherData.getImg());
                teacherData.setFavorito(true);
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                onSelectAction.FavoriteAddOrRemove(teacherData.getNome(), teacherData.getCognome(),
                        teacherData.getRuolo(), false, teacherData.getId(), teacherData.getSito(),
                        teacherData.getImg());
                teacherData.setFavorito(false);
            }
        });
    }

    @Override
    public int getItemCount() {
        return teacherList.size();
    }

    class DownloadFilesTask extends AsyncTask<String, Void, Bitmap> {
        MyViewHolder holder;
        TeacherData teacherData;


        public DownloadFilesTask(MyViewHolder holder, TeacherData teacherData) {
            this.holder = holder;
            this.teacherData = teacherData;
        }

        @Override
        protected Bitmap doInBackground(String... params) {
            return downImage(params[0], 1000);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            /*progDialog = new ProgressDialog(getContext());
            progDialog.setMessage("Caricamento");
            progDialog.setIndeterminate(false);
            progDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progDialog.setCancelable(true);
            progDialog.show();*/

        }

        @Override
        protected void onPostExecute(Bitmap s) {
            super.onPostExecute(s);
            s = getRoundedShape(s);
            holder.image.setImageBitmap(s);
            teacherData.setImg(s);
        }

        private Bitmap downImage(String urlStr, int timeOutRequest) {
            HttpURLConnection urlConnection = null;
            Bitmap imageCyrcular = null;
            try {
                final String methodConnection = "SET";
                final URL url = new URL(urlStr);

                urlConnection = (HttpURLConnection) url.openConnection();

                // imposta la connessione
                //urlConnection.setRequestMethod(methodConnection);
                // urlConnection.setRequestProperty("Content-length", "0");
                //urlConnection.setUseCaches(false);
                //urlConnection.setAllowUserInteraction(false);
                //urlConnection.setConnectTimeout(1000);
                //urlConnection.setReadTimeout(timeOutRequest);

                // apre la connessione
                urlConnection.connect();

                // stato connessione http
                int status = urlConnection.getResponseCode();

                switch (status) {
                    case 200:
                    case 201:
                        InputStream stream = urlConnection.getInputStream();
                        imageCyrcular = BitmapFactory.decodeStream(stream);
                        break;

                    default:
                        //null
                        break;
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
            }
            return imageCyrcular;
        }
    }

    public static Bitmap getRoundedShape(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();

        return output;

        /*int targetWidth = 86;
        int targetHeight = 86;
        Bitmap targetBitmap = Bitmap.createBitmap(targetWidth,
                targetHeight,Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(targetBitmap);
        Path path = new Path();
        path.addCircle(((float) targetWidth - 1) / 2,
                ((float) targetHeight - 1) / 2,
                (Math.min(((float) targetWidth),
                        ((float) targetHeight)) / 2),
                Path.Direction.CCW);

        canvas.clipPath(path);
        canvas.drawBitmap(scaleBitmapImage,
                new Rect(0, 0, scaleBitmapImage.getWidth(),
                        scaleBitmapImage.getHeight()),
                new Rect(0, 0, targetWidth, targetHeight), null);

        return targetBitmap;*/
    }
}