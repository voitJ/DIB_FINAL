package com.example.danie.dibapp.PianoStudi;

import android.view.View;
import android.widget.TextView;

import com.example.danie.dibapp.R;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

/**
 * Created by Alessandro on 22/01/17.
 */

public class YearViewHolder extends GroupViewHolder {

    private TextView yearNumber;

    public YearViewHolder(View itemView) {
        super(itemView);

        yearNumber = (TextView) itemView.findViewById(R.id.yearNumber);
        yearNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down, 0);
    }


    @Override
    public void expand() {
        yearNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_down, 0);

    }

    @Override
    public void collapse() {

        yearNumber.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_keyboard_arrow_up, 0);
    }

    public void setGroupName(ExpandableGroup group) {
        yearNumber.setText(group.getTitle());
    }
}
