package com.example.danie.dibapp.Charts;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.danie.dibapp.MainActivity;
import com.example.danie.dibapp.R;

import java.util.ArrayList;
import java.util.List;


public class ChartsFragment extends Fragment {
    private final static String TAG = ChartsFragment.class.getName();

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private float elevation;
    private AppBarLayout appbar;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((MainActivity) getActivity()).getSupportActionBar().setTitle("Grafici");
        appbar = (AppBarLayout) getActivity().findViewById(R.id.appbar);
        elevation = appbar.getElevation();
        appbar.setElevation(0);


        View view = inflater.inflate(R.layout.fragment_graphs, container, false);

        viewPager = (ViewPager) view.findViewById(com.example.danie.dibapp.R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(com.example.danie.dibapp.R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        //setupTabIcons();


        return view;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        appbar.setElevation(elevation);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        appbar.setElevation(elevation);
    }



    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getActivity().getSupportFragmentManager());
        adapter.addFragment(new ColumnChart_Fragment(), "trend voti");
        adapter.addFragment(new MeanFragment(), "variazione medie");
        adapter.addFragment(new PieChart_Fragment(), "frequenza voti");
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);

    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}