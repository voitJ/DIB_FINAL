package com.example.danie.dibapp.rss;

public class RssItem {

    private final String title;
    private final String date;
    private final String link;
    private final String description;


    public RssItem(String title, String link, String date, String description) {
        this.title = title;
        this.date = date;
        this.link = link;
        this.description = description;

    }

    public String getTitle() { return title; }

    public String getDate() { return date; }

    public String getLink() { return link; }

    public String getDescription() {
        return description;
    }


}
