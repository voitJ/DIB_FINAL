package com.example.danie.dibapp.rss;

import android.app.IntentService;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.os.ResultReceiver;
import android.util.Log;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.text.ParseException;
import java.util.List;

public class RssService extends IntentService {

    private static final String TAG = RssService.class.getName();

    private static final String RSS_LINK = "http://www.uniba.it/ricerca/dipartimenti/informatica/search_rss?portal_type=News%20zona&path=/uniba4/ricerca/dipartimenti/informatica";
    public static final String ITEMS = "items";
    public static final String RECEIVER = "receiver";

    public RssService() {
        super ("RssService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "Inizio servizio");
        List<RssItem> rssItems = null;
        if (checkInternetConnection()) {
            try {
                UnibaRssParser parser = new UnibaRssParser();
                try {
                    rssItems = parser.parse(getInputStream(RSS_LINK));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } catch (XmlPullParserException e) {
                Log.w(e.getMessage(), e);
            } catch (IOException e) {
                Log.w(e.getMessage(), e);
            }
            Bundle bundle = new Bundle();
            bundle.putSerializable(ITEMS, (Serializable) rssItems);
            ResultReceiver reciver = intent.getParcelableExtra(RECEIVER);
            reciver.send(0, bundle);
        }else {
            System.out.println("Not Connected");
            Bundle bundle = new Bundle();
            bundle.putSerializable(ITEMS, (Serializable) rssItems);
            ResultReceiver reciver = intent.getParcelableExtra(RECEIVER);
            reciver.send(0, bundle);
        }
    }

    //check internet connection
    private boolean checkInternetConnection() {
        // Connectivity Manager serve per controllare la connnessione
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(getBaseContext().CONNECTIVITY_SERVICE);

        //controllo la connessione al network
        if (connectivityManager.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(0).getState() == NetworkInfo.State.CONNECTING ||
                connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTING ||
                connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
            System.out.println("Connected");
            return true;
        }else if (connectivityManager.getNetworkInfo(0).getState() == NetworkInfo.State.DISCONNECTED ||
                connectivityManager.getNetworkInfo(1).getState() == NetworkInfo.State.CONNECTED) {
            System.out.println("Not Connected");
            return false;
        }
        return false;
    }

    public InputStream getInputStream(String link) {
        try {
            URL url = new URL(link);
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            Log.w(TAG, "Eccezione sollevata durante la ricezione dei dati", e);
            return null;
        }
    }
}
