package com.example.danie.dibapp.fragments;


import android.graphics.Bitmap;
import  android.support.v4.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;
import com.example.danie.dibapp.Data.TeacherData;
import com.example.danie.dibapp.R;
import com.example.danie.dibapp.SearchActivity;
import com.example.danie.dibapp.Data.OnselectAction;
import com.example.danie.dibapp.database.TeacherDbHelper;
import java.util.List;
import com.example.danie.dibapp.Data.DividerItemDecoration;
import com.example.danie.dibapp.Data.ListTeacherAdapter;


public class SearchFragment extends Fragment {

    private ListTeacherAdapter mAdapterAll;
    public OnFavoritePress mCallBack;
    public getTeacherDataList nCallBack;
    public OnselectAction aCallBack;
    private RecyclerView recyclerView;
    private List<TeacherData> teacherDataList;
    private TeacherDbHelper teacherDbHelper;
    private Bitmap image;

    // Container Activity must implement this interface
    public interface OnFavoritePress {
        void viewMessageOnSnackBar(String name, String surname, int status);
    }

    public interface getTeacherDataList {
        List<TeacherData> getList();
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        return super.onContextItemSelected(item);
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallBack = (OnFavoritePress) activity;
            nCallBack = (getTeacherDataList) activity;
            aCallBack = (OnselectAction) activity;


        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFavoritePress");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /*Bundle mBundle = new Bundle();
        mBundle = getArguments();
        int test = 0;
        if (mBundle != null) {
            test = mBundle.getInt("prova");
        }


        if (test == 1) {
            mCallBack.refreshJson();
        }*/

        teacherDbHelper = new TeacherDbHelper(getContext());
        teacherDataList = nCallBack.getList();
        if (teacherDataList.isEmpty()) {
            mCallBack.viewMessageOnSnackBar(null, null, 2);
        }

        //riempi la lista con il dati dal json

        /*DownloadFilesTask dwlm = new DownloadFilesTask();
        try {
            dwlm.execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }*/


        //async task deve effettuare tutte le operazioni in background
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate layout
        View root = inflater.inflate(R.layout.fragment_search, container, false);

        recyclerView = (RecyclerView) root.findViewById(R.id.recycler_view);
        mAdapterAll = new ListTeacherAdapter(teacherDataList, this.aCallBack,this.getContext());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(root.getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(root.getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapterAll);

        /*//PULL REFRESH
        final SwipeRefreshLayout refreshlayot = (SwipeRefreshLayout) root.findViewById(R.id.swipeRefreshLayout);
        refreshlayot.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //ricarica la lista dal server
                mCallBack.refreshJson();

                //svuota la lista della View con i "vecchi dati"
                teacherDataList.clear();

                //ricarica la lista con i dati dal Json
                //setListFromJson();

                DownloadFilesTask dwlm = new DownloadFilesTask();
                dwlm.execute();


                //notifica all'adapter che i dati sono cambiati
                mAdapterAll.notifyDataSetChanged();

                //fine dell'animazione
                refreshlayot.setRefreshing(false);
            }
        });*/
        return root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        //imposta il listener sullo spinner
        MenuItem item = menu.findItem(R.id.menu_spinner_favorite);
        Spinner spinner = (Spinner) MenuItemCompat.getActionView(item);
        setspinnerListener(spinner);
    }

    private void setspinnerListener(Spinner spinner) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1) {

                    //passa a visulizzare tutti cambiando la struttura collegata all'adapter
                    spinnerChange();
                    Toast.makeText(getActivity(), "preferiti", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    private void spinnerChange() {
        FavoriteFragment fragment = new FavoriteFragment();
        ((SearchActivity) getActivity()).fragChange(null,fragment);
    }
}
