package com.example.danie.dibapp.Libretto;

/**
 * Created by Alessandro on 16/01/17.
 */

public class Exam {

    private String name;
    private int grade;
    private int credits;
    private String date;
    private String professor;
    private String notes;

    public Exam() {
    }

    public Exam(String name, int grade, int credits, String date, String professor, String notes) {
        this.name = name;
        this.grade = grade;
        this.credits = credits;
        this.date = date;
        this.professor = professor;
        this.notes = notes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getProfessor() {
        return professor;
    }

    public void setProfessor(String professor) {
        this.professor = professor;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
