package com.example.danie.dibapp.PianoStudi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.danie.dibapp.Data.DividerItemDecoration;
import com.example.danie.dibapp.MainActivity;
import com.example.danie.dibapp.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class PianoStudiFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private String userId;
    ArrayList<Course> courseArrayList = new ArrayList<Course>();
    TextView mEmptyView;
    RecyclerAdapter adapter;
    private RecyclerView recyclerView;
    private ArrayList<Year> years;
    static FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mDatabaseReference;
    ArrayList<Course> firstYear = new ArrayList<>();
    ArrayList<Course> secondYear = new ArrayList<>();
    ArrayList<Course> thirdYear = new ArrayList<>();

    private static FloatingActionButton fab;

    public PianoStudiFragment() {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MainActivity) getActivity())
                .setActionBarTitle("Piano di studi");
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static PianoStudiFragment newInstance(int columnCount) {
        PianoStudiFragment fragment = new PianoStudiFragment();

        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        database.setPersistenceEnabled(true);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_piano_di_studi, container, false);
        setHasOptionsMenu(true);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.couseRecyclerView);


        if (mRecyclerView != null) {

            mRecyclerView.setHasFixedSize(true);
        }


        years = new ArrayList<>();
        mDatabaseReference = database.getReference();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child("pianoStudi").child("ITPS");
        mDatabaseReference.addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot examSnapshot : dataSnapshot.getChildren()) {

                            Course course = examSnapshot.getValue(Course.class);
                            int year = course.getYear();

                            if (year == 1) firstYear.add(course);
                            else if (year == 2) secondYear.add(course);
                            else thirdYear.add(course);

                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }


                });
        //setData();
        years.add(new Year("Primo anno", firstYear));
        years.add(new Year("Secondo anno", secondYear));
        years.add(new Year("Terzo anno", thirdYear));

        mLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);


        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                mLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);


        adapter = new RecyclerAdapter(getActivity(), years);
        mRecyclerView.setAdapter(adapter);


        return view;
    }

}