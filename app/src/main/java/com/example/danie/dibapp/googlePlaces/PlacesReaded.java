package com.example.danie.dibapp.googlePlaces;

import android.os.AsyncTask;
import android.util.Log;

import com.example.danie.dibapp.fragments.RunMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;

public class PlacesReaded extends AsyncTask<String, Integer, String> {

    private final static String TAG = PlacesReaded.class.getName();

    String data = null;

    @Override
    protected String doInBackground(String... url) {
        Log.i(TAG, ":entrato in doInBackground()");
        try{
            data = downloadUrl(url[0]);
        }catch(Exception e){
            Log.d("Background Task",e.toString());
        }
        Log.i(TAG, data);
        return data;
    }

    @Override
    protected void onPostExecute(String result){
        Log.i(TAG, ":entrato in onPostExecute()");
        ParserTask parserTask = new ParserTask();

        // Inizio analisi Google places nel formato JSON
        // Invoco "doInBackground()" della classe ParseTask
        parserTask.execute(result);
    }

    /** Download json dall'url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);

            // Creo una connessione http per comunicare con l'url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connessione all'url
            urlConnection.connect();

            // Lettura dati dall'url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb  = new StringBuffer();
            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }
            data = sb.toString();
            Log.e("data", ">>>>" + data);
            br.close();

        }catch(Exception e){
            Log.d("Eccezzione durante il download url", e.toString());
        }finally{
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /** Classe di analisi Google places nel formato JSON */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>{

        JSONObject jObject;

        // Invocato da execute()
        @Override
        protected List<HashMap<String,String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;
            JSONParser jsonParser = new JSONParser();

            try{
                jObject = new JSONObject(jsonData[0]);

                /** Prendo i dati analizzati come una lista di costruttori */
                places = jsonParser.parse(jObject);

            }catch(Exception e){
                Log.d("Exception",e.toString());
            }
            return places;
        }

        // Eseguito dopo il metodo doInBackground()
        @Override
        protected void onPostExecute(List<HashMap<String,String>> list){
            RunMapFragment.mMap.clear();

            for(int i = 0; i < list.size(); i++){
                MarkerOptions markerOptions = new MarkerOptions();

                // Get di un posto da una lista
                HashMap<String, String> hmPlace = list.get(i);
                double lat = Double.parseDouble(hmPlace.get("lat"));
                double lng = Double.parseDouble(hmPlace.get("lng"));
                String name = hmPlace.get("place_name");
                String vicinity = hmPlace.get("vicinity");

                LatLng latLng = new LatLng(lat, lng);

                markerOptions.position(latLng);
                markerOptions.title(name + " : " + vicinity);
                RunMapFragment.mMap.addMarker(markerOptions);
            }
        }
    }
}
