package com.example.danie.dibapp;


import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.danie.dibapp.Authentication.SignupActivity;
import com.example.danie.dibapp.Bluetooth.BluetoothExplore;
import com.example.danie.dibapp.Charts.ChartsFragment;
import com.example.danie.dibapp.Libretto.LibrettoFragment;
import com.example.danie.dibapp.PianoStudi.PianoStudiFragment;
import com.example.danie.dibapp.fragments.DashboardFragment;
import com.example.danie.dibapp.fragments.NewsFragment;
import com.example.danie.dibapp.fragments.RunMapFragment;
import com.example.danie.dibapp.fragments.dummy.DummyContent;
import com.example.danie.dibapp.utility.MyDatabaseUtil;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.FirebaseDatabase;

import static com.facebook.login.widget.ProfilePictureView.TAG;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, LibrettoFragment.OnListFragmentInteractionListener {


    public static Toolbar toolbar;
    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;
    public static android.support.v4.app.FragmentManager sFm;
    private SupportMapFragment sMapFragment;
    private SharedPreferences pref;
    private static final String SHARED_PREFERENCES_TYPE = "Account";
    FirebaseUser user;
    //dichiarazioni Firebase
    static FirebaseDatabase database;
    private String userId;
    private Uri photoUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        database = MyDatabaseUtil.getDatabase();
        user = FirebaseAuth.getInstance().getCurrentUser();
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null) {

            Log.d(TAG, "fullname main" + bundle.getString("fullname"));

            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName(bundle.getString("fullname"))
                    .build();
            user.updateProfile(profileUpdates);


        }


        String fullName = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
        if (fullName == null) fullName = SignupActivity.getFullname(); //caso di registrazione
        String userEmail = FirebaseAuth.getInstance().getCurrentUser().getEmail();
        setContentView(com.example.danie.dibapp.R.layout.activity_main);
        toolbar = (Toolbar) findViewById(com.example.danie.dibapp.R.id.toolbar);


        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        drawer = (DrawerLayout) findViewById(com.example.danie.dibapp.R.id.drawer_layout);
        userId = user.getUid();

        NavigationView navigationView = (NavigationView) findViewById(com.example.danie.dibapp.R.id.nav_view);
        View header = navigationView.getHeaderView(0);

        TextView nav_fullname = (TextView) header.findViewById(R.id.nav_fullname);
        nav_fullname.setText(fullName);
        TextView nav_email = (TextView) header.findViewById(R.id.nav_email);
        nav_email.setText(userEmail);
        final ImageView profile = (ImageView) header.findViewById(R.id.user_profile_photo);

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, com.example.danie.dibapp.R.string.navigation_drawer_open, com.example.danie.dibapp.R.string.navigation_drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {


                photoUrl = Uri.parse("https://firebasestorage.googleapis.com/v0/b/dibapp-b8fda.appspot.com/o/profile_icon.png?alt=media&token=cf3742c4-acb6-4c2d-b2ae-a516569aa082");
                if (user != null && user.getPhotoUrl() != null) {
                    photoUrl = user.getPhotoUrl();
                }

                pref = getSharedPreferences(SHARED_PREFERENCES_TYPE, MODE_PRIVATE);
                String localStr = pref.getString(userId, null);
                if (localStr != null) {
                    Uri localUrl = Uri.parse(localStr);
                    if (localUrl != photoUrl) {
                        photoUrl = localUrl;


                    }

                }

                Glide.with(getApplicationContext())
                        .load(photoUrl)
                        //.skipMemoryCache(true)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(profile);

            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }
        };

        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        if (savedInstanceState == null) {
            sMapFragment = SupportMapFragment.newInstance();

            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(com.example.danie.dibapp.R.id.content_frame, new DashboardFragment()).commit();
        }
    }


    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(com.example.danie.dibapp.R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.example.danie.dibapp.R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        if (id == com.example.danie.dibapp.R.id.save_exam) {
            return true;
        }


        return super.onOptionsItemSelected(item);


    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        FragmentManager fm = getSupportFragmentManager();
        sFm = getSupportFragmentManager();

        int id = item.getItemId();

        if (sMapFragment != null && sMapFragment.isAdded())
            sFm.beginTransaction().hide(sMapFragment).commit();

        if (id == com.example.danie.dibapp.R.id.nav_news) {

            toolbar.getMenu().clear();
            toolbar.inflateMenu(com.example.danie.dibapp.R.menu.main);


            getSupportFragmentManager().beginTransaction().replace(com.example.danie.dibapp.R.id.content_frame, new NewsFragment()).commit();


        } else if (id == com.example.danie.dibapp.R.id.nav_pianoStudi) {

            toolbar.getMenu().clear();
            toolbar.inflateMenu(com.example.danie.dibapp.R.menu.main);


            /*Intent intent = new Intent(this, PianoDiStudiActivity.class);
            startActivity(intent);*/


            getSupportFragmentManager().beginTransaction().replace(com.example.danie.dibapp.R.id.content_frame, new PianoStudiFragment()).commit();


        } else if (id == com.example.danie.dibapp.R.id.nav_libretto) {

            toolbar.getMenu().clear();
            toolbar.inflateMenu(com.example.danie.dibapp.R.menu.main);


            getSupportFragmentManager().beginTransaction().replace(com.example.danie.dibapp.R.id.content_frame, new LibrettoFragment()).commit();


        } else if (id == R.id.nav_grafici) {

            toolbar.getMenu().clear();
            toolbar.inflateMenu(com.example.danie.dibapp.R.menu.main);

            getSupportFragmentManager().beginTransaction().replace(com.example.danie.dibapp.R.id.content_frame, new ChartsFragment()).commit();


        } else if (id == com.example.danie.dibapp.R.id.nav_maps) {

            toolbar.getMenu().clear();
            toolbar.inflateMenu(com.example.danie.dibapp.R.menu.menu_maps);


            getSupportFragmentManager().beginTransaction().replace(com.example.danie.dibapp.R.id.content_frame, new RunMapFragment()).commit();


        } else if (id == R.id.nav_dashboard) {

            toolbar.getMenu().clear();
            toolbar.inflateMenu(R.menu.main);


            getSupportFragmentManager().beginTransaction().replace(com.example.danie.dibapp.R.id.content_frame, new DashboardFragment()).commit();


        } else if (id == com.example.danie.dibapp.R.id.nav_prof) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_explore) {
            Intent intent = new Intent(this, BluetoothExplore.class);
            startActivity(intent);
        }


        setTitle(item.getTitle());

        DrawerLayout drawer = (DrawerLayout) findViewById(com.example.danie.dibapp.R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void onListFragmentInteraction(DummyContent.DummyItem uri) {
        //you can leave it empty
    }
}
