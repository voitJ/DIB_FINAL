package com.example.danie.dibapp.Libretto;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.example.danie.dibapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class EditExamActivity extends AppCompatActivity {


    private TextInputLayout inputLayoutExam;
    private TextInputLayout inputLayoutVote;
    private TextInputLayout inputLayoutCFU;
    private TextInputLayout inputLayoutProfessor;
    private TextInputLayout inputLayoutNotes;
    private TextInputLayout inputLayoutDate;
    private EditText inputVote;
    private EditText inputCFU;
    private EditText inputDate;
    private String rvStringDate;
    private final String myStringFormat = "EEEE dd MMMM yyyy";
    private final String recyclerViewStringFormat = "dd/MM/yyyy";
    private Date rvDate;
    private final SimpleDateFormat myFormat = new SimpleDateFormat(myStringFormat, Locale.ITALY);
    private final SimpleDateFormat rvFormat = new SimpleDateFormat(recyclerViewStringFormat, Locale.ITALY);
    private boolean idoneo = false;
    private boolean lode = false;
    private String key;
    private DatabaseReference mDatabaseReference;
    private String userId;
    String dbStringFormat = "yyyy/MM/dd";
    SimpleDateFormat dbFormat = new SimpleDateFormat(dbStringFormat, Locale.ITALY);

    public EditExamActivity() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_add_exam);

        mDatabaseReference = FirebaseDatabase.getInstance().getReference();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();

        Toolbar myToolbar = (Toolbar) findViewById(R.id.addExam_toolbar);
        setSupportActionBar(myToolbar);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeAsUpIndicator(R.drawable.ic_clear_icon);
        ab.setTitle("Modifica esame");

        key = getIntent().getStringExtra("key");
        inputLayoutVote = (TextInputLayout) findViewById(R.id.voteWrapper);
        inputVote = (EditText) findViewById(R.id.vote_edittext);

        Log.e("Chat", "voto: " + getIntent().getStringExtra("grade"));

        if (getIntent().getStringExtra("grade").equals(" ID")) {
            inputVote.setText(R.string.idoneo);
            idoneo = true;
        } else if (getIntent().getStringExtra("grade").equals(" L")) {
            inputVote.setText(R.string.lode);
            lode = true;
        } else {

            inputVote.setText(getIntent().getStringExtra("grade"));
        }


        inputVote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater inflaters = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflaters.inflate(R.layout.number_picker, null);
                final NumberPicker numberPicker = (NumberPicker) view.findViewById(R.id.numberPicker1);

                AlertDialog.Builder builder = new AlertDialog.Builder(EditExamActivity.this);
                builder.setView(view);

                final String[] values = {"Idoneo", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27",
                        "28", "29", "30", "30L"};
                numberPicker.setDisplayedValues(values);
                numberPicker.setMaxValue(14);
                numberPicker.setMinValue(0);
                numberPicker.setWrapSelectorWheel(true);


                builder.setTitle(R.string.select_vote)

                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {


                            @Override
                            public void onClick(DialogInterface dialog, int id) {


                                switch (numberPicker.getValue()) {

                                    case 0:
                                        inputVote.setText(R.string.idoneo);
                                        lode = false;
                                        idoneo = true;
                                        break;

                                    case 14:
                                        inputVote.setText(R.string.lode);
                                        lode = true;
                                        idoneo = false;
                                        break;

                                    default:
                                        inputVote.setText(String.valueOf(numberPicker.getValue() + 17));
                                        lode = idoneo = false;
                                }


                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });

                final AlertDialog dialog = builder.create();
                dialog.show();


            }
        });


        inputLayoutExam = (TextInputLayout) findViewById(R.id.examWrapper);
        final EditText inputExam = (EditText) findViewById(R.id.exam_edittext);
        inputExam.setText(getIntent().getStringExtra("name"));


        inputLayoutProfessor = (TextInputLayout) findViewById(R.id.professorWrapper);
        final EditText inputProfessor = (EditText) findViewById(R.id.professor_edittext);
        inputProfessor.setText(getIntent().getStringExtra("professor"));

        inputLayoutNotes = (TextInputLayout) findViewById(R.id.notesWrapper);
        final EditText inputNotes = (EditText) findViewById(R.id.notes_edittext);
        inputNotes.setText(getIntent().getStringExtra("notes"));


        inputLayoutCFU = (TextInputLayout) findViewById(R.id.cfuWrapper);
        inputCFU = (EditText) findViewById(R.id.cfu_edittext);

        String toChange = getIntent().getStringExtra("credits");
        String correct;
        if (toChange.length() == 6) {
            correct = toChange.substring(0, 2);
        } else {
            correct = String.valueOf(toChange.charAt(0));
        }


        inputCFU.setText(correct);
        inputCFU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LayoutInflater inflaters = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflaters.inflate(R.layout.number_picker, null);
                final NumberPicker numberPicker = (NumberPicker) view.findViewById(R.id.numberPicker1);

                AlertDialog.Builder builder = new AlertDialog.Builder(EditExamActivity.this);
                builder.setView(view);

                numberPicker.setMaxValue(12);
                numberPicker.setMinValue(3);
                numberPicker.setWrapSelectorWheel(true);


                builder.setTitle(R.string.select_cfu)

                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {


                            @Override
                            public void onClick(DialogInterface dialog, int id) {


                                inputCFU.setText(String.valueOf(numberPicker.getValue()));


                            }
                        })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });

                final AlertDialog dialog = builder.create();
                dialog.show();


            }
        });

        inputLayoutDate = (TextInputLayout) findViewById(R.id.dateWrapper);
        inputDate = (EditText) findViewById(R.id.date_edittext);


        String dateStr = getIntent().getStringExtra("date");

        try {
            rvDate = rvFormat.parse(dateStr);
            rvStringDate = dbFormat.format(rvDate);


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (rvDate != null) {
            String listDate = myFormat.format(rvDate);
            inputDate.setText(listDate);
        }


        inputDate.setOnClickListener(new View.OnClickListener() {


            final Calendar myCalendar = Calendar.getInstance();

            final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                @Override
                public void onDateSet(DatePicker view, int year, int monthOfYear,
                                      int dayOfMonth) {
                    // TODO Auto-generated method stub
                    myCalendar.set(Calendar.YEAR, year);
                    myCalendar.set(Calendar.MONTH, monthOfYear);
                    myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                    rvStringDate = dbFormat.format(myCalendar.getTime());


                    inputDate.setText(myFormat.format(myCalendar.getTime()));
                }

            };


            @Override

            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(EditExamActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });


        inputExam.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (TextUtils.isEmpty(s)) {
                    inputLayoutExam.setError("Inserire esame");
                } else {
                    inputLayoutExam.setError(null);


                }

            }
        });


        inputVote.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (TextUtils.isEmpty(s)) {
                    inputLayoutVote.setError("Selezionare voto");
                } else {
                    inputLayoutVote.setError(null);


                }

            }
        });


        inputCFU.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (TextUtils.isEmpty(s)) {

                    inputLayoutCFU.setError("Selezionare CFU");
                } else {
                    inputLayoutCFU.setError(null);


                }

            }
        });


        inputDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (TextUtils.isEmpty(s)) {
                    inputLayoutDate.setError("Selezionare data");
                } else {
                    inputLayoutDate.setError(null);


                }

            }
        });


    }


    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu_edit, menu);

        // menu.getItem(0).setEnabled(false);
        return true;


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        switch (item.getItemId()) {

            case R.id.save_exam:


                String exam = inputLayoutExam.getEditText().getText().toString();
                String grade;
                if (idoneo) grade = "17";
                else if (lode) grade = "31";
                else grade = inputVote.getText().toString();
                String credits = inputCFU.getText().toString().concat(" CFU");
                String date = inputDate.getText().toString();
                String professor = inputLayoutProfessor.getEditText().getText().toString();
                String notes = inputLayoutNotes.getEditText().getText().toString();


                if (exam.equals("") || grade.equals("") || (!grade.equals("17") && credits.length() == 4) || date.equals("")) {

                    if (exam.equals("")) inputLayoutExam.setError("Inserisci esame");
                    if (grade.equals("")) inputLayoutVote.setError("Seleziona voto");
                    if (credits.length() == 4) inputLayoutCFU.setError("Seleziona CFU");
                    if (date.equals("")) inputLayoutDate.setError("Seleziona data");


                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setMessage(R.string.fill_required_fields)
                            .setCancelable(false)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    //do things
                                    dialog.dismiss();
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                    break;


                } else {


                    Exam mNewExam = new Exam(exam, Integer.parseInt(grade), Integer.parseInt(inputCFU.getText().toString()), rvStringDate, professor, notes);
                    mDatabaseReference.child("users").child(userId).child("exams").push().setValue(mNewExam);

                    Toast toast = Toast.makeText(getApplicationContext(), R.string.exam_edited, Toast.LENGTH_LONG);
                    toast.show();

                    Intent intent = new Intent();
                    intent.putExtra("key", key);
                    setResult(1, intent);
                    this.finish();
                }


            default:
                setResult(2);
                this.finish();

        }
        return true;
    }


}