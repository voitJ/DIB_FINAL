package com.example.danie.dibapp.rss;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by enric on 02/06/2016.
 */
public class UnibaRssParser {

    private final String ns = null;
    private final String myStringFormat = "dd MMMM";
    private final String title_exeption = "Dipartimento di Informatica";

    public List<RssItem> parse(InputStream inputStream) throws XmlPullParserException, IOException, ParseException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(inputStream, null);
            parser.nextTag();
            return readerFeed(parser);
        } finally {
            inputStream.close();
        }
    }

    private List<RssItem> readerFeed(XmlPullParser parser) throws XmlPullParserException, IOException, ParseException {
        parser.require(XmlPullParser.START_TAG, null, "rdf:RDF");
        String title = null;
        String link = null;
        String date = null;
        String description = null;
        List<RssItem> items = new ArrayList<RssItem>();
        while (parser.next() != XmlPullParser.END_DOCUMENT) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("title")) {
                title = readTitle(parser);
            } else if (name.equals("link")) {
                link = readLink(parser);
            } else if (name.equals("dc:date")) {
                String dbStringDate = readDate(parser);

                String dbStringFormat = "yyyy-MM-dd";
                SimpleDateFormat dbFormat = new SimpleDateFormat(dbStringFormat, Locale.ITALY);
                SimpleDateFormat myFormat = new SimpleDateFormat(myStringFormat, Locale.ITALY);

                Date dbDate = dbFormat.parse(dbStringDate);
                date = myFormat.format(dbDate);


                // date = date.substring(0, 10);
            } else if (name.equals("description"))
                description = readDescription(parser);
            if (title != null && link != null && date != null && description != null) {
                RssItem item = new RssItem(title, link, date, description);
                items.add(item);
                title = null;
                link = null;
                date = null;
                description = null;
            }
        }
        return items;
    }

    private String readLink(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "link");
        String link = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "link");
        return link;
    }

    private String readTitle(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "title");
        String title = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "title");
        return title;
    }

    private String readDate(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "dc:date");
        String date = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "dc:date");
        return date;
    }

    private String readDescription(XmlPullParser parser) throws XmlPullParserException, IOException {
        parser.require(XmlPullParser.START_TAG, ns, "description");
        String description = readText(parser);
        parser.require(XmlPullParser.END_TAG, ns, "description");
        return description;
    }

    private String readText(XmlPullParser parser) throws XmlPullParserException, IOException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
}
