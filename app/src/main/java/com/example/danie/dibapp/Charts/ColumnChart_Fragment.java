package com.example.danie.dibapp.Charts;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.danie.dibapp.Libretto.Exam;
import com.example.danie.dibapp.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lecho.lib.hellocharts.listener.ColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.ColumnChartView;


public class ColumnChart_Fragment extends Fragment {


    private ColumnChartView chart;
    private ColumnChartData data;
    private boolean hasAxes = true;
    private boolean hasAxesNames = false;
    private boolean hasLabels = false;
    private boolean hasLabelForSelected = false;
    private int numColumns;
    private String userId;
    static FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mDatabaseReference;
    final int columnColor = Color.parseColor("#ffb03b");
    private SimpleDateFormat dbFormat = new SimpleDateFormat("yyyy/MM/dd");
    SimpleDateFormat listFormat = new SimpleDateFormat("dd/MM/yy");
    private ArrayList<Integer> grades = new ArrayList<Integer>();
    private ArrayList<String> dates = new ArrayList<String>();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        userId = user.getUid();

    }

    public ColumnChart_Fragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        View rootView = inflater.inflate(R.layout.fragment_column_chart, container, false);

        chart = (ColumnChartView) rootView.findViewById(R.id.chart);
        chart.setOnValueTouchListener(new ValueTouchListener());


        mDatabaseReference = database.getReference();
        mDatabaseReference = FirebaseDatabase.getInstance().getReference().child("users").child(userId).child("exams");
        mDatabaseReference.orderByChild("date").addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot examSnapshot : dataSnapshot.getChildren()) {

                            Exam exam = examSnapshot.getValue(Exam.class);
                            if (exam.getGrade() != 17) {
                                int grade = exam.getGrade();
                                if (grade == 31) grade = 30;
                                grades.add(grade);
                                String date = exam.getDate();

                                Date data = null;
                                try {
                                    data = dbFormat.parse(date);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                date = listFormat.format(data);
                                dates.add(date);
                                numColumns = grades.size();
                                generateData();
                                resetViewPort();

                            }


                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        //handle databaseError
                    }


                });


        return rootView;


    }


    private void generateData() {


        int numSubcolumns = 1;
        List<Column> columns = new ArrayList<Column>();
        List<SubcolumnValue> values;
        for (int i = 0; i < numColumns; ++i) {

            values = new ArrayList<SubcolumnValue>();
            for (int j = 0; j < numSubcolumns; ++j) {
                values.add(new SubcolumnValue((float) grades.get(i), columnColor));
            }

            Column column = new Column(values);
            column.setHasLabels(hasLabels);
            column.setHasLabelsOnlyForSelected(hasLabelForSelected);
            columns.add(column);
        }

        data = new ColumnChartData(columns);

        if (hasAxes) {
            List<AxisValue> axisXValuesWeight = new ArrayList<>();
            for (int i = 0; i < numColumns; i++)
                axisXValuesWeight.add(new AxisValue(i).setLabel(dates.get(i)));
            Axis axisX = new Axis(axisXValuesWeight);
            axisX.setHasLines(true);
            axisX.setMaxLabelChars(8);
            axisX.setHasTiltedLabels(true);

            List<AxisValue> axisValuesWeight = new ArrayList<>();  //it's Y axis value list
            for (int i = 18; i < 31; i++)
                axisValuesWeight.add(new AxisValue(i));
            Axis axisY = new Axis(axisValuesWeight);
            axisY.setHasLines(true);
            if (hasAxesNames) {
                axisX.setName("Axis X");
                axisY.setName("Axis Y");
            }
            data.setAxisXBottom(axisX);
            data.setAxisYLeft(axisY);
        } else {
            data.setAxisXBottom(null);
            data.setAxisYLeft(null);
        }

        chart.setColumnChartData(data);


    }


    private class ValueTouchListener implements ColumnChartOnValueSelectListener {

        @Override
        public void onValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value) {
            Toast.makeText(getActivity(), "Selected: " + value, Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onValueDeselected() {
            // TODO Auto-generated method stub

        }

    }


    private void resetViewPort() {

        Viewport v = new Viewport(chart.getMaximumViewport());
        v.bottom = 17;
        v.top = 30;
        v.left = -0.5f;

        if (grades.size() < 11) {
            v.right = 9.5f;
        } else {
            v.right = (float) (grades.size() - 0.5);
        }

        chart.setMaximumViewport(v);
        chart.setCurrentViewport(v);
    }


}