package com.example.danie.dibapp.PianoStudi;

import android.view.View;
import android.widget.TextView;

import com.example.danie.dibapp.R;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

/**
 * Created by Alessandro on 22/01/17.
 */

public class CourseViewHolder extends ChildViewHolder {

    private TextView courseName;
    private TextView courseCredits;
    private TextView courseProfessor;

    public CourseViewHolder(View itemView) {
        super(itemView);

        courseName = (TextView) itemView.findViewById(R.id.courseName);
        courseCredits = (TextView) itemView.findViewById(R.id.courseCredits);
        courseProfessor = (TextView) itemView.findViewById(R.id.courseProfessor);

    }


    public void onBind(Course course, ExpandableGroup group) {
        courseName.setText(course.getName());
        courseCredits.setText(String.valueOf(course.getCredits()) + " CFU");
        if (String.valueOf(course.getCredits()).equals("0")) {
            courseCredits.setText("Idoneità");
        }
        courseProfessor.setText(course.getProfessor());
//if (course.getProfessor().equals("Da definire")) courseProfessor.setTextColor();
    }


}
