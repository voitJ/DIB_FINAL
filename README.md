# DIBAPP

## Descrizione
L'Applicazione sviluppata per la piattaforma android permette di gestire un 
libretto universitario. 




## Traccia DIB app

-Libretto universitario e sua personalizzazione con uno dei loghi 
dell’università o con altre immagini

-Piano di studi, con CFU, data dell’appello sostenuto, nome docente, 
eventuali note; voto (inclusivo di voti non numerici, p.e. Idoneità, Ritirato, 

-Calcolo automatico e istantaneo di: media aritmetica e pesata (sui CFU), 
tempo di permanenza in università (in anni), 
calcolo del probabile mese di Laurea, 

-numero di crediti già superati e ancora da superare, indicazione di voto 
minimo necessario per ottenere un certo voto di laurea

-Grafici automatici (es. esami sostenuti nel tempo, trend dei voti nel tempo, 
frequenza dei singoli voti)

-Comunicazioni relative alle lezioni, agli esami ed a opportunità 
per gli studenti (seminari, incontri, ...)

-Accesso a video/podcast dell'Università per scaricare le lezioni disponibili

-News di ateneo e relativi al proprio Corso di Laurea

-Contatti dei docenti e delle segreterie di ciascun Corso di Laurea

-Indicazioni per raggiungere le mense convenzionate e i servizi 
d'ateneo con l'ausilio di mappe

-Prenotazione degli esami

-Informazioni sul Dipartimento, sui Corsi di Studio, sui servizi offerti 
dall'Università/dal Dipartimento, su iniziative per l’orientamento allo studio

-Informazioni su posti nei dintorni dove: mangiare, fare fotocopie/stampare tesi, 
acquistare biglietto dell'autobus, comprare un libro, comprare cancelleria, ...

-Condivisione materiale multimediale attinente alla vita universitaria

-Ricerca di studenti, docenti, personale
Prima della realizzazione dell'app consultare qualcuna delle svariate
app esistenti relative al mondo universitario 
(es. italiane: Unikore, Bicocca, Bocconi, Sapienza, Unipi, PoliBA; straniere: 
Harvard, Stanford, Missouri, Cambridge, Nebraska,Virginia)